/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Businesslogic;
import Transformation.Data_Transformer;
import Transformation.dataProfiler;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import persitentstorage.dbhandler;
import twitter4j.TwitterException;

/**
 *
 * @author sibghat
 */
public class systemasministratoroffice {
private usercatalogue object=new usercatalogue(); 
public ArrayList<user> getlist() throws SQLException, ClassNotFoundException
{
 return object.getuserlist();
}

 public int adduser(String na,String cnic,String number,String address,String password)
{
    try
    {
     usercatalogue obj=new usercatalogue();
     obj.adduser(na,cnic, number, address,password);
     return 1;
    }
     catch (Exception e)
             {
             return 0;
             }
}
public int deleteuser(String id, String pass) throws SQLException, ClassNotFoundException
{
        usercatalogue obj=new usercatalogue();
        return obj.deleteuser(id,pass);
}
public int verifyuser(String id,String pass)
{
    
        usercatalogue obj=new usercatalogue();
       
      
           return obj.verifyuser(id,pass);
     
}

public int updateuser(String na,String id,String cnic,String number,String address,String password) throws SQLException
{
    try
    {
    usercatalogue obj=new usercatalogue();
    obj.updateuser(na, id,cnic, number,address,password);
    return 1;
}
    catch (Exception e)
    {
        return 0;
    }
    
}
int addData(String str , String d1, String d2) throws ClassNotFoundException, SQLException, IOException, TwitterException, Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
// dbhandler db=new dbhandler();         // un comment to pupolate a tables data from  previous to bewer one
// db.addDuplicateData1();
        socialNetworks sn = new socialNetworks();
        sn.acquireData(str, d1, d2);
        sn.acquireDatafb(str, d1, d2);
        System.out.println("Adding data done");       
     return 1;
    }

   ArrayList<dataProfiler> profile_data() throws ClassNotFoundException, SQLException
   {
       //ArrayList<String> lis=new ArrayList<>();
       ArrayList<dataProfiler> list=new ArrayList<>();
       System.out.println("1");
       dataProfiler dpf=new dataProfiler();
       /////////////////Twitter Data Profiling //////////////
     //  lis.add("Null value for Tweet Id :"+dpf.count_null("twitter_data","tweet_id")) ;
     //  lis.add("Null value for Text :"+dpf.count_null("twitter_data","text"));
       dataProfiler dpf1=new dataProfiler("Text",dpf.count_null("twitter_data","text"),dpf.unique_values("twitter_data","text"));
       list.add(dpf1);
       
     //  lis.add("Null value for User Name :"+dpf.count_null("twitter_data","user_name"));
       dataProfiler dpf2=new dataProfiler("User Name",dpf.count_null("twitter_data","user_name"),dpf.unique_values("twitter_data","user_name"));
       list.add(dpf2);
       
     //  lis.add("Null value for Favorited Count :"+dpf.count_null("twitter_data","favorited_count"));
       dataProfiler dpf3=new dataProfiler("Favourite Count",dpf.count_null("twitter_data","favorited_count"),dpf.unique_values("twitter_data","favorited_count"));
       list.add(dpf3);
       
     //  lis.add("Null value for Retweeted Count :"+dpf.count_null("twitter_data","retweeted_count"));
       dataProfiler dpf4=new dataProfiler("Retweet Count",dpf.count_null("twitter_data","retweeted_count"),dpf.unique_values("twitter_data","retweeted_count"));
       list.add(dpf4);
       
     //  lis.add("Null value for Language :"+dpf.count_null("twitter_data","language"));
       dataProfiler dpf5=new dataProfiler("Language",dpf.count_null("twitter_data","language"),dpf.unique_values("twitter_data","language"));
       list.add(dpf5);
       
     //  lis.add("Null value for Location :"+dpf.count_null("twitter_data","location"));
       dataProfiler dpf6=new dataProfiler("Location",dpf.count_null("twitter_data","location"),dpf.unique_values("twitter_data","location"));
       list.add(dpf6);
       
       //lis.add("Null value for Is Retwwted :"+dpf.count_null("twitter_data","is_retweeted"));
       
       
     //  lis.add("Unique value for Tweet Id :"+dpf.unique_values("twitter_data","tweet_id"));
     //  lis.add("Unique value for Text :"+dpf.unique_values("twitter_data","text"));
     //  lis.add("Unique value for User Name :"+dpf.unique_values("twitter_data","user_name"));
     //  lis.add("Unique value for Favorited Count :"+dpf.unique_values("twitter_data","favorited_count"));
     //  lis.add("Unique value for Retwweted Count :"+dpf.unique_values("twitter_data","retweeted_count"));
     //  lis.add("Unique value for Language :"+dpf.unique_values("twitter_data","language"));
     //  lis.add("Unique value for Location :"+dpf.unique_values("twitter_data","location"));
     //  lis.add("Unique value for Is retweeted :"+dpf.unique_values("twitter_data","is_retweeted"));
     //////////////////////////////////////////////////////facebokk data profiling//////////////
     ///////////////////////////////////////////////////////////////////////////////////////////
      return list;
   }
   public void transform_data() throws ClassNotFoundException, SQLException, IOException, Exception
   {
       Data_Transformer dtfrm=new Data_Transformer();
//     dtfrm.set_lookup_tables();  // old implementation
       dtfrm.set_language_location();   // standardize location and KPI's             & populating transformed tables
//     dtfrm.Classifier();    // new N-gram and Bi-Gram classifier implementation UnComment to train your classifier
       dtfrm.analyze_data();
     System.out.println("Data Transformation Completely Done");     
   }
   public void loadingDimensionModel() throws ClassNotFoundException, SQLException, IOException, Exception
   {
       Data_Transformer dtfrm=new Data_Transformer();
       dtfrm.populatingDimentionTables();
   }
   public void businessQuestions() throws ClassNotFoundException, SQLException, IOException, Exception
   {
        Data_Transformer dtfrm=new Data_Transformer();
        dtfrm.businessQuestionsDatapopulation();
   }
}


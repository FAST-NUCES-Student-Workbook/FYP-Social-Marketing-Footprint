/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;

import persitentstorage.dbhandler;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sibghat
 */
public class usercatalogue {

    private ArrayList<user> userlis = new ArrayList<>();

    public usercatalogue() {
            userlis= new ArrayList<>();
    }

    public int adduser(String na, String cnic, String phone, String address, String passwrod) {
        try {
            user cuser = new user(na, cnic, phone, address, passwrod);
            dbhandler obj = dbhandler.getInstance();
            userlis.clear();
            obj.adduser(cuser);
            userlis = obj.populateUserList();
            return 1;
        } catch (Exception e) {
            return 0;
        }
    }

    public int deleteuser(String id,String pass) throws SQLException, ClassNotFoundException {
           
            user cuser = new user(id,pass);
            dbhandler obj = dbhandler.getInstance();
           int a= obj.delete(cuser);
            userlis.clear();
            userlis = obj.populateUserList();
        return a;
    }
    

    public int verifyuser(String id, String pass) {
        int x = 0;
        try {
            dbhandler obj = dbhandler.getInstance();
            userlis.clear();
            userlis = obj.populateUserList();
            
            for (user userlis1 : userlis) {
                if (userlis1.getid().equals(id)) {
                    if (userlis1.getpassword().equals(pass)) {
                        x = 1;
                        System.out.println("value of x: " + String.valueOf(x));
                        return x;
                    } else {
                        x = 0;
                        System.out.println("value of x: " + String.valueOf(x));
                        return x;
                    }
                }
                 System.out.println("value of x: " + String.valueOf(x));
            }
        } catch (Exception e) {

            return 0;
        }
        
        return x;
    }

    public int updateuser(String na, String id, String cnic, String number, String address, String password) {
        try {
            user cuser = new user(id, na, cnic, number, address, password);
            dbhandler obj = dbhandler.getInstance();
            obj.update(cuser);
            userlis.clear();
            userlis=obj.populateUserList();

            return 1;
        } catch (Exception e) {
            return 0;
        }

    }

    public ArrayList<user> getuserlist() throws SQLException, ClassNotFoundException {

       dbhandler obj = dbhandler.getInstance();
            userlis.clear();
            userlis = obj.populateUserList();
            
        return this.userlis;
    }
}

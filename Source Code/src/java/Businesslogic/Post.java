/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Businesslogic;

import java.sql.Date;

/**
 *
 * @author Abubaker
 */
public class Post {
    private String postID;
    private String postText;
    private int sharesCount;
    private int likeCounts;
    private int nofComments;
    private Date createdTime; 
    private String telcoName;

    public Post() {
    }

    public Post(String postID, String postText, int sharesCount, int likeCounts, int nofComments, Date createdTime, String telcoName) {
        this.postID = postID;
        this.postText = postText;
        this.sharesCount = sharesCount;
        this.likeCounts = likeCounts;
        this.nofComments = nofComments;
        this.createdTime = createdTime;
        this.telcoName = telcoName;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public void setSharesCount(int sharesCount) {
        this.sharesCount = sharesCount;
    }

    public void setLikeCounts(int likeCounts) {
        this.likeCounts = likeCounts;
    }

    public void setNofComments(int nofComments) {
        this.nofComments = nofComments;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public void setTelcoName(String telcoName) {
        this.telcoName = telcoName;
    }

    public String getPostID() {
        return postID;
    }

    public String getPostText() {
        return postText;
    }

    public int getSharesCount() {
        return sharesCount;
    }

    public int getLikeCounts() {
        return likeCounts;
    }

    public int getNofComments() {
        return nofComments;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getTelcoName() {
        return telcoName;
    }

    
}

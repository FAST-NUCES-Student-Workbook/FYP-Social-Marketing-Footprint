/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import twitter4j.TwitterException;
import javax.servlet.http.HttpSession;



/**
 *
 * @author Aqeel's
 */
@WebServlet(name = "indexservlet",
        urlPatterns = {"/addacc",
            "/verify",
            "/index",
            "/update",
            "/AcquireData",
            "/ViewData",
            "/delete",
            "/AnalyzeData"})
public class guiServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
    */
    
 //     PostJSONImp postImplObj=new PostJSONImp();
    String inputKeywords = new String();
 //   ArrayList<String> myArrayList = new ArrayList<>();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String userPath = request.getServletPath();
            System.out.println(userPath);
            if(userPath.equals("/index")) 
            {
                System.out.println("redirected from index");
                if (request.getParameter("usertype").equals("admin") && request.getParameter("username").equals("admin") && request.getParameter("userpass").equals("admin")) {
                    //request.getRequestDispatcher("admin.jsp").forward(request, response); 
                    System.out.println("user is admin");
                   getServletContext().setAttribute("userid", request.getParameter("username"));
                   response.sendRedirect("AdminHome.jsp");
                } 
                else 
                {
                    System.out.println("user is not admin");
                    String username;
                    username = request.getParameter("username");
                    String password = request.getParameter("userpass");
                    systemasministratoroffice obj = new systemasministratoroffice();
                    int verifyuser = obj.verifyuser(username, password);
                    if (verifyuser == 1) {
                        
                        getServletContext().setAttribute("userid", username);
                        
                        response.sendRedirect("BusinessAnalyst.jsp");
                    } else {
                        System.out.println("verifyuser");
                        System.out.println(verifyuser);
                        response.sendRedirect("index.jsp");
                    }
                }

            } else if (userPath.equals("/addacc")) {
                System.out.println("redirected from AddAccount");
                String password = request.getParameter("userpass");
                String name = request.getParameter("username");
                String cnic = request.getParameter("usercnic");
                String address = request.getParameter("useraddr");
                String phone = request.getParameter("userphone");

                System.out.println("Add Account");

                systemasministratoroffice sa = new systemasministratoroffice();
                sa.adduser(name, cnic, phone, address, password);
                response.sendRedirect("ManageAccounts.jsp");

            } else if (userPath.equals("/update")) {
                System.out.println("redirected from Update");
                String id = request.getParameter("userid");
                String password = request.getParameter("userpass");
                String name = request.getParameter("username");
                String cnic = request.getParameter("usercnic");
                String address = request.getParameter("useraddr");
                String phone = request.getParameter("userphone");

                System.out.println("Update Account");

                systemasministratoroffice sa = new systemasministratoroffice();
                try {
                    System.out.println("USER ID: "+id);
                    sa.updateuser(name, id, cnic, phone, address, password);
                    response.sendRedirect("ManageAccounts.jsp");
                } catch (SQLException ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (userPath.equals("/delete")) {
                System.out.println("redirected from Delete");
                String id = request.getParameter("userid");
                String password = request.getParameter("userpass");

                System.out.println("Delete Account");

                systemasministratoroffice sa = new systemasministratoroffice();
                int b=sa.verifyuser(id, password);
                        
                if(b==1)
                {
                    try {
                        int deleteuser = sa.deleteuser(id, password);
                        System.out.println("Deleted!");
                        response.sendRedirect("ManageAccounts.jsp");
                    } catch (SQLException ex) {
                        Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
            } else if (userPath.equals("/AcquireData")) {
                System.out.println("redirected from AcquireData");
                systemasministratoroffice sao = new systemasministratoroffice();
//                try {
                      inputKeywords = request.getParameter("keyword");
                      String date1=request.getParameter("date1");
                      String date2=request.getParameter("date2");
//                      try {
//    Thread.sleep(6000);                 //1000 milliseconds is one second.
//} catch(InterruptedException ex) {
//    Thread.currentThread().interrupt();
//}
                    ///////////////////////////////////// Twitter Data Extraction ////////////////////////////

                    int check=sao.addData(inputKeywords,date1,date2);
                request.setAttribute("outputstring", "Done");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/AcquireData.jsp");
            rd.forward(request, response);
                    
//                } catch (SQLException ex) {
//                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (TwitterException ex) {
//                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (Exception ex) {
//                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
                else if (userPath.equals("/AnalyzeData")) {
                System.out.println("redirected from AnalyzeData");
//                try {
//    Thread.sleep(10000);                 //1000 milliseconds is one second.
//} catch(InterruptedException ex) {
//    Thread.currentThread().interrupt();
//}
//                RequestDispatcher rd = getServletContext().getRequestDispatcher("/AnalyzeData.jsp");
//            rd.forward(request, response);
//     
                systemasministratoroffice sao = new systemasministratoroffice();
                try {
                     sao.transform_data();
                     sao.loadingDimensionModel();
                     sao.businessQuestions();
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/AnalyzeData.jsp");
            rd.forward(request, response);
            
                } catch (SQLException ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                }  catch (Exception ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
              }

             else if (userPath.equals("/verify")) {
                System.out.println("redirected from verify");
                String id = request.getParameter("userid");
                String pass = request.getParameter("userpass");
                ArrayList<user> list = new ArrayList<>();
                systemasministratoroffice sa = new systemasministratoroffice();
                try {
                    list = sa.getlist();
                    for (user list1 : list) {
                        if (list1.getid().equals(id) && list1.getpassword().equals(pass)) {

                            request.setAttribute("Uid", list1.getid());
                            request.setAttribute("Uname", list1.getname());
                            request.setAttribute("Upass", list1.getpassword());
                            request.setAttribute("Unumber", list1.getnumber());
                            request.setAttribute("Uaddress", list1.getaddress());
                            request.setAttribute("Ucnic", list1.getcnic());
                            //System.out.println(list1.getname());
                            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ManageAccounts.jsp");
                            rd.forward(request, response);
                            break;
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
             else if (userPath.equals("/ViewData")) {
                System.out.println("redirected from ViewData");
                systemasministratoroffice sao = new systemasministratoroffice();
                try {
                      
                   
                     
//                    ArrayList<ArrayList<String>> post= sao.getTransformedPosts();
//                    ArrayList<ArrayList<String>> comment= sao.getTransformedComments();
//                    ArrayList<ArrayList<String>> tweet= sao.getTransformedTweets();
                    
                  ArrayList<Transformation.dataProfiler> list=sao.profile_data();
                    
                  ArrayList<ArrayList<String>> list1= new ArrayList<>();
                  
                  for (Transformation.dataProfiler l:list)
                  {
                      ArrayList<String> list2= new ArrayList<>();
                      list2.add(l.getFieldName());
                      list2.add(l.getNullCount());
                      list2.add(l.getUniqueCount());
                      list1.add(list2);
                  }
 
                    request.setAttribute("list", list1);
                  
                        RequestDispatcher rd = getServletContext().getRequestDispatcher("/ViewData.jsp");
                        rd.forward(request, response);
                    
                    
                    
                } catch (SQLException ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else
            {
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/error.jsp");
                            rd.forward(request, response);
            }
//        } catch (InterruptedException ex) {
//            Logger.getLogger(indexservlet.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (SQLException ex) {
//            Logger.getLogger(indexservlet.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (TwitterException ex) {
//            Logger.getLogger(indexservlet.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(indexservlet.class.getName()).log(Level.SEVERE, null, ex);
//        }
        } catch (SQLException ex) {
            Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TwitterException ex) {
            Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(guiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
     /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
 
    }
       @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
    
   


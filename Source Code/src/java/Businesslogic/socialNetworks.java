/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.json.Json;
import javax.json.stream.JsonParser;

import persitentstorage.dbhandler;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author sibghat
 */
public class socialNetworks {

    int LIMIT = 1000; //the number of retrieved tweets 
    ConfigurationBuilder cb;
    Twitter twitter;

    String inputKeywords, pageId = new String();
    ArrayList<String> myArrayList = new ArrayList<>();
    ArrayList<Post> postListObj = new ArrayList<>();
    ArrayList<Feed> feedListObj = new ArrayList<>();
    int pid=0,pidtw = 0;
    String accessTokenString = "313143458890041|pzTUEsFmwUH1gf9DM2erfA2ykQA";

    public socialNetworks() {
        cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey("APGMnR9C4evzmxuxckZdktAYm");
        cb.setOAuthConsumerSecret("vKzLjXEVNy3JsD21NxhHYy5KJToYEg9DPyxHVWZx0kDMYY33GC");
        cb.setOAuthAccessToken("1295610061-wkG2BoYoatGyDjzjHu4dGLFLIQ8xqKKezXuV0Ns");
        cb.setOAuthAccessTokenSecret("HyOqLT0P2zeLa5MzOS9enUtZpUHiQqHe1Rydthv8Z2CXD");
        twitter = new TwitterFactory(cb.build()).getInstance();
    }

    public void acquireData(String input, String d1, String d2) throws InterruptedException, IOException, SQLException, TwitterException, ClassNotFoundException, Exception {
       System.out.println("socialNetworks.java");
       dbhandler db = dbhandler.getInstance();
       pidtw=db.getPostCounter("twitter_data","t");
        ArrayList<tweet> list = new ArrayList<>();
        Query query = new Query(input);

        query.setSince(d1); // yyyy-mm-dd
        query.getUntil();
 //     query.setUntil("2014-05-01");
        query.setCount(100);

        QueryResult r;
        do {
            r = twitter.search(query);
            ArrayList ts = (ArrayList) r.getTweets();
            for (Object t1 : ts) {
                pidtw++;
                tweet rd = new tweet();
                Status t = (Status) t1;
                rd.setText(t.getText());
                //     rd.setText(tan.Translate(t.getText(),t.getLang()));   // skipping Translation to English
                rd.setTweet_id("t"+pidtw);
                rd.setName(t.getUser().getScreenName());
                rd.setLanguage(t.getLang());
                rd.setLocation(t.getUser().getLocation());
                rd.setFavorited_count(Integer.toString(t.getFavoriteCount()));
                rd.setRetweeted_count(Integer.toString(t.getRetweetCount()));
                if (t.isRetweet() == true) {
                    rd.setIs_retweeted(1);
                }
                Date time = t.getCreatedAt();
                java.sql.Date created_date = new java.sql.Date(time.getYear(), time.getMonth(), time.getDay());
                rd.setCreated_date(created_date);
                switch (input) {
                    case "Mobilink":
                    case "mobilink":
                        rd.setTelcoName("M1");
                        break;
                    case "Telenor":
                    case "telenor":
                        rd.setTelcoName("T2");
                        break;
                    case "Ufone":
                    case "ufone":
                        rd.setTelcoName("U3");
                        break;
                    case "Zong":
                    case "zong":
                        rd.setTelcoName("Z4");
                        break;
                    case "Warid":
                    case "warid":
                        rd.setTelcoName("W5");
                        break;
                }
 //               System.out.println(created_date); // To check DTA of which date is comming
                list.add(rd);
            }
        } while ((query = r.nextQuery()) != null);
        //sao.addData(list);      

 
        db.addData(list);
    }

    void acquireDatafb(String inputKeywords, String d1, String d2) throws MalformedURLException, IOException, ParseException, ClassNotFoundException, SQLException, Exception {
        //       throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.            
       dbhandler db = dbhandler.getInstance();
       pid=db.getPostCounter("post","f");
       
        URL url = new URL("https://graph.facebook.com/search?q=" + inputKeywords + "&type=page&fields=id,name&limit=10&access_token=" + accessTokenString);

        try (InputStream is = url.openStream();
                JsonParser parser = Json.createParser(is)) {
            System.out.println("inside try");
            while (parser.hasNext()) {
                JsonParser.Event e = parser.next();
                if (e == JsonParser.Event.KEY_NAME) {

                    switch (parser.getString()) {
                        case "id":
                            parser.next();
                            myArrayList.add(parser.getString());
                            System.out.println("Servlet gettingdataServlet at " + parser.getString());
                            break;
                        default:
                            parser.next();
                            break;
                    }
                }
            }
            is.close();
        } catch (Exception e) {
            System.out.println(e);
        }
//  int i=0;

        for (String myArrayList1 : myArrayList) {
            pageId = myArrayList1;
            System.out.println(pageId);
            URL url2 = new URL("https://graph.facebook.com/" + pageId + "/posts?until="+d2+"&since="+d1+"&access_token=" + accessTokenString);
//////  Making Object if JSON from urls
            InputStream is2 = new URL(url2.toString()).openStream();
            try {
                BufferedReader rdr = new BufferedReader(new InputStreamReader(is2, Charset.forName("UTF-8")));
                String jsonText = readAll(rdr);
                JSONObject json = new JSONObject(jsonText);

                JSONArray list = json.getJSONArray("data");
                final int size = list.length();
                for (int i = 0; i < size; i++) {                    // populating each post data one by one
                    JSONObject postJSONObject = list.getJSONObject(i);
                    init(postJSONObject, inputKeywords);
                }
            } catch (JSONException e) {
                System.out.println(e);
            }
            System.out.println("\n" + " New Page data" + "\n \n");
        } // end for loop

        populateTables();
    }

    public void init(JSONObject json, String telcoName) throws JSONException, ParseException, MalformedURLException, IOException, Exception {
        Post postObj = new Post();

        try {
            pid++;
            postObj.setPostID("P" + pid);
            postObj.setPostText((getRawString("message", json)));
            //       postObj.setPostText(tan.TranslateFBStatus(getRawString("message", json)));
            String id = getRawString("id", json);
            //           System.out.println(postObj.getPostText());   //// for checking data                 

            if (!json.isNull("shares")) {
                JSONObject sharesJSONObject = json.getJSONObject("shares");
                if (!sharesJSONObject.isNull("count")) {
                    postObj.setSharesCount(getInt("count", sharesJSONObject));
//                    System.out.println(getInt("count", sharesJSONObject));     /////// for printing and checking                 
                }

            }
            if (!json.isNull("likes")) {

                URL url4 = new URL("https://graph.facebook.com/" + id + "/likes?" + accessTokenString + "&limit=5000");

                InputStream is4 = new URL(url4.toString()).openStream();
                try {
                    BufferedReader rdr = new BufferedReader(new InputStreamReader(is4, Charset.forName("UTF-8")));
                    String jsonText = readAll(rdr);
                    JSONObject likesjson = new JSONObject(jsonText);

                    JSONArray list = likesjson.getJSONArray("data");
                    final int size = list.length();
                    postObj.setLikeCounts(size);
                } catch (JSONException e) {
                }
            }

            if (!json.isNull("comments")) {
                URL url3 = new URL("https://graph.facebook.com/" + id + "/comments?" + accessTokenString + "&limit=5000");

                InputStream is2 = new URL(url3.toString()).openStream();
                try {
                    BufferedReader rdr = new BufferedReader(new InputStreamReader(is2, Charset.forName("UTF-8")));
                    String jsonText = readAll(rdr);
                    JSONObject commentsjson = new JSONObject(jsonText);

                    JSONArray commentlist = commentsjson.getJSONArray("data");
                    final int size = commentlist.length();
                    postObj.setNofComments(size);
                    for (int i = 0; i < size; i++) {
                        Feed feedObj = new Feed();
                        feedObj.setPostID(postObj.getPostID());
                        JSONObject commentJSONObject = commentlist.getJSONObject(i);
                        feedObj.setCommentText((getRawString("message", commentJSONObject)));
                        //                    feedObj.setCommentText(tan.TranslateFBStatus(getRawString("message", commentJSONObject)));
        //                System.out.println(postObj.getPostID() + "-" + feedObj.getCommentText()); // to check 
                        feedObj.setCommentLikes(getInt("like_count", commentJSONObject));
                        feedListObj.add(feedObj);
                    }
                } catch (JSONException e) {

                }
            }

            postObj.setCreatedTime(getISO8601Datetime("created_time", json)); // format = yyyy-MM-dd
            switch (telcoName) {
                case "Mobilink":
                case "mobilink":
                    postObj.setTelcoName("M1");
                    break;
                case "Telenor":
                case "telenor":
                    postObj.setTelcoName("T2");
                    break;
                case "Ufone":
                case "ufone":
                    postObj.setTelcoName("U3");
                    break;
                case "Zong":
                case "zong":
                    postObj.setTelcoName("Z4");
                    break;
                case "Warid":
                case "warid":
                    postObj.setTelcoName("W5");
                    break;
            }
            postListObj.add(postObj);

        } catch (JSONException e) {

        }
    }

    public void populateTables() throws ClassNotFoundException, SQLException {
        dbhandler dbh = dbhandler.getInstance();
        //    persistentstorage.DBHandler dbh=new persistentstorage.DBHandler();
        dbh.addnewPosts(postListObj);
        dbh.addnewFeeds(feedListObj);
    }

    public static Calendar ConverttoDate1(String d, String format) throws ParseException {
        Calendar mydate = new GregorianCalendar();
        java.util.Date thedate = new SimpleDateFormat(format, Locale.ENGLISH).parse(d);
        mydate.setTime(thedate);
        return mydate;
    }

// function for setting date and time
    public static java.sql.Date getISO8601Datetime(String name, JSONObject json) throws ParseException {
        String d = getRawString(name, json);
        d = d.substring(0, 10);
        java.sql.Date createdTime = null;
        ////////////////////////////
        if (d.contains("-")) {
            String format = "yyyy-MM-dd";
            Calendar mydate = ConverttoDate1(d, format);
            int year = mydate.get(Calendar.YEAR) - 1900;
            int month = mydate.get(Calendar.MONTH);
            int day = mydate.get(Calendar.DAY_OF_MONTH);
            createdTime = new java.sql.Date(year, month, day);
            return createdTime;
        } else if (d.contains("/")) {
            String format = "yyyy/MM/dd";
            Calendar mydate = ConverttoDate1(d, format);
            int year = mydate.get(Calendar.YEAR) - 1900;
            int month = mydate.get(Calendar.MONTH);
            int day = mydate.get(Calendar.DAY_OF_MONTH);
            createdTime = new java.sql.Date(year, month, day);
            return createdTime;
        }
        return createdTime;
//        
//        if (dateString == null) {
//            return null;
//        }
//        if (json.isNull("timezone")) {
//            return parseISO8601Date(dateString);
//        } else {
//            TimeZone timezone = getTimeZone("timezone", json);
//            return parseISO8601Date(dateString, timezone);
//        }
    }
// function getting integer

    public static Integer getInt(String name, JSONObject json) {
        return getInt(getRawString(name, json));
    }

    public static Integer getInt(String str) {
        if (null == str || "".equals(str) || "null".equals(str)) {
            return null;
        } else {
            try {
                return Integer.valueOf(str);
            } catch (NumberFormatException nfe) {
                // workaround for the API side issue http://twitter4j.org/jira/browse/TFJ-484
                return null;
            }
        }
    }
// function of reading Raw String

    public static String getRawString(String name, JSONObject json) {
        try {
            if (json.isNull(name)) {
                return null;
            } else {

                return json.getString(name);
            }
        } catch (JSONException jsone) {
            return null;
        }
    }
//    public static TimeZone getTimeZone(String name, JSONObject json) {
//        if (json.isNull(name)) {
//            return null;
//        }
//        return TimeZone.getTimeZone(getRawString(name, json));
//    }
//
//    private static java.sql.Date parseISO8601Date(String dateString) {
//        return parseISO8601Date(dateString, TimeZone.getTimeZone("UTC"));
//    }
//
//    private static java.sql.Date parseISO8601Date(String dateString, TimeZone timezone) {
//        try {
//            return (java.sql.Date) new SimpleDateFormat(ISO8601_DATE_FORMAT).parse(dateString);
//        } catch (ParseException e1) {
//            try {
//                SimpleDateFormat sdf = new SimpleDateFormat(ISO8601_DATE_FORMAT_WITHOUT_TZ);
//                sdf.setTimeZone(timezone);
//                return (java.sql.Date) sdf.parse(dateString);
//            } catch (ParseException e2) {
//                try {
//                    SimpleDateFormat sdf = new SimpleDateFormat(ISO8601_DATE_FORMAT_WITHOUT_TIME);
//                    sdf.setTimeZone(timezone);
//                    return (java.sql.Date) sdf.parse(dateString);
//                } catch (ParseException e3) {
//                    return null;
//                }
//            }
//        }
//    }

    private static String readAll(Reader rdr) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rdr.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}

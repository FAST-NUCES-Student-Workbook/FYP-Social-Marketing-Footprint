/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesslogic;

import java.util.Date;

/**
 *
 * @author sibghat
 */
public class tweet {
private String tweet_id;
private String text;
private String name;
private String favorited_count;
private String retweeted_count;
private String language;
private String location;
private int is_retweeted;
private Date created_date;
private String telcoName;

public tweet()
{
    is_retweeted=0;
}

    public tweet(String tweet_id, String text, String name, String favorited_count, String retweeted_count, String language, String location, int is_retweeted, Date created_date, String telcoName) {
        this.tweet_id = tweet_id;
        this.text = text;
        this.name = name;
        this.favorited_count = favorited_count;
        this.retweeted_count = retweeted_count;
        this.language = language;
        this.location = location;
        this.is_retweeted = is_retweeted;
        this.created_date = created_date;
        this.telcoName = telcoName;
    }

    public void setTweet_id(String tweet_id) {
        this.tweet_id = tweet_id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFavorited_count(String favorited_count) {
        this.favorited_count = favorited_count;
    }

    public void setRetweeted_count(String retweeted_count) {
        this.retweeted_count = retweeted_count;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setIs_retweeted(int is_retweeted) {
        this.is_retweeted = is_retweeted;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public void setTelcoName(String telcoName) {
        this.telcoName = telcoName;
    }

    public String getTweet_id() {
        return tweet_id;
    }

    public String getText() {
        return text;
    }

    public String getName() {
        return name;
    }

    public String getFavorited_count() {
        return favorited_count;
    }

    public String getRetweeted_count() {
        return retweeted_count;
    }

    public String getLanguage() {
        return language;
    }

    public String getLocation() {
        return location;
    }

    public int getIs_retweeted() {
        return is_retweeted;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public String getTelcoName() {
        return telcoName;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Businesslogic;

/**
 *
 * @author Abubaker
 */
public class Feed {
    private String postID;
    private String commentText;
    private int commentLikes;


    public Feed() {
    }

    public Feed(String postID, String commentText, int commentLikes) {
        this.postID = postID;
        this.commentText = commentText;
        this.commentLikes = commentLikes;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getCommentLikes() {
        return commentLikes;
    }

    public void setCommentLikes(int commentLikes) {
        this.commentLikes = commentLikes;
    }   
}

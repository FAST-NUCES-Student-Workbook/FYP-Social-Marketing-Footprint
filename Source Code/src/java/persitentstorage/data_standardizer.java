/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persitentstorage;

import com.aliasi.classify.ConditionalClassification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import net.java.frej.fuzzy.Fuzzy;

/**
 *
 * @author Aqeel's
 */
public class data_standardizer {

    private static data_standardizer instance;
    String[] categories;
    LMClassifier classifier;
    String[] categories2;
    LMClassifier classifier2;

    public static synchronized data_standardizer getInstance() {
        if (instance == null) {
            instance = new data_standardizer();
        }
        return instance;
    }

    public String set_language(String lang) {
        if (null != lang) {
            switch (lang) {
                case "en":
                    lang = "English";
                    break;
                case "in":
                    lang = "Urdu/Hindi";
                    break;
                case "no":
                    lang = "Norwegian";
                    break;
                case "fr":
                    lang = "French";
                    break;
                case "gr":
                    lang = "German";
                    break;
                case "po":
                    lang = "Portugeze";
                    break;
                case "br":
                    lang = "Brazilian";
                    break;
            }
        }

        return lang;
    }

    //////////////////Calculate The strength of the specific tweet on the basis N-Gram and Bofram///////////////
    public double calculate_strength(String sentence) throws ClassNotFoundException, SQLException, IOException {
        double d = 0.0;
        if (sentence != null) {
            //       System.out.println(sentence);
            d = this.final_classification(sentence);
        }
        return d;
    }

    public String classify(String text) throws IOException, ClassNotFoundException {

        ConditionalClassification classification = classifier.classify(text);
        return classification.bestCategory();
    }

    public String classify2(String text) {
        ConditionalClassification classification = classifier2.classify(text);
        return classification.bestCategory();
    }

    public double final_sentiment(String query) throws IOException, ClassNotFoundException {

        double answr = 0.0;
        String[] temps = query.split("[\\W]");
        if (temps.length > 2) 
        {
            for (int i = 0; i < temps.length - 1; i++) 
            {
                answr = answr + this.scaling(this.classify2(temps[i] + "   " + temps[i + 1]));
            }
            double db = temps.length - 1;
            answr=answr/db;
            double answr1 = this.scaling1(this.classify(query));
            answr = 0.7 * answr + 0.3 * answr1;
            return answr;
        } 
        else 
        {
            answr = this.scaling(this.classify2(query));
            double answr1 = this.scaling1(this.classify(query));
            answr = 0.7 * answr + 0.3 * answr1;
            return answr;
        }
    }

    public double scaling(String s) {
        Double result = 0.0;
        if (s.equals("Negative")) {
            result = -2.12;
        }
        if (s.equals("Very Negative")) {
            result = -4.25;
        }
        if (s.equals("Neutral")) {
            result = 0.0;
        }
        if (s.equals("Positive")) {
            result = 2.12;
        }
        if (s.equals("Very Positive")) {
            result = 4.25;

        }
        return result;
    }

    public double scaling1(String s) {
        double db = 0.0;
        if (s.equals("neg")) {
            db = -6.0;
        }
        if (s.equals("pos")) {
            db = 6.0;
        }
        return db;
    }

    public double final_classification(String query) throws IOException, ClassNotFoundException {

        classifier = (LMClassifier) AbstractExternalizable.readObject(new File("D:\\Practice\\Net beans\\SMF\\classified1.txt"));
        categories = classifier.categories();
        classifier2 = (LMClassifier) AbstractExternalizable.readObject(new File("D:\\Practice\\Net beans\\SMF\\classified2.txt"));
        categories2 = classifier2.categories();

        double sentiment = this.final_sentiment(query);
        if (sentiment > 3.5) {
            sentiment = 5.0;
        }
        if (sentiment < 3.5 && sentiment > 1) {
            sentiment = 4.0;
        }
        if (sentiment < 1 && sentiment > -1) {
            sentiment = 3.0;
        }
        if (sentiment < -1 && sentiment > -3.5) {
            sentiment = 2.0;
        }
        if (sentiment < -3.5 && sentiment > -6.0) {
            sentiment = 1.0;
        }
        return sentiment;
    }

    public String set_location(String loc, ArrayList<String> cityNames) {
        for (String tok : cityNames) {
            if (loc.equals(tok)) {
                return tok;
            }
        }

        ArrayList<String> cityName2 = new ArrayList<>();
        cityName2.add("Islamabad");
        cityName2.add("Lahore");
        cityName2.add("Karachi");
        cityName2.add("Rawalpindi");
        cityName2.add("Peshawar");
        cityName2.add("Quetta");
        cityName2.add("Faisalabad");
        cityName2.add("Multan");
        cityName2.add("Bahawalpur");
        cityName2.add("Gujranwala");
        cityName2.add("Dera Ghazi Khan");
        cityName2.add("Sialkot");
        cityName2.add("Hyderabad");

        SecureRandom r = new SecureRandom();
        int x = r.nextInt(13);
        return cityName2.get(x);
    }

    public String set_text(String sentence) {
        if (" ".equals(sentence)) {
            sentence = " This was a blank tweet ";
        }
        return sentence;
    }
////////////Function used to get synonymus of a word ///////////////////////

    public ArrayList<String> get_synonyms(String word) {

        ArrayList<String> lis = new ArrayList<>();
        System.setProperty("wordnet.database.dir", "C:\\Program Files (x86)\\WordNet\\2.1\\dict");
        WordNetDatabase database = WordNetDatabase.getFileInstance();
        Synset[] synsets = database.getSynsets(word);
        if (synsets.length > 0) {
            for (Synset synset : synsets) {

                String[] wordForms = synset.getWordForms();
                lis.addAll(Arrays.asList(wordForms));
            }
        }
        return lis;
    }

    public String remove_punctuation(String input) {

        String alpha = input.replaceAll("[^a-zA-Z\\s]", "");
        java.util.StringTokenizer tokenizerr = new java.util.StringTokenizer(alpha.trim(), " ");
        StringBuilder temp = new StringBuilder();
        while (tokenizerr.hasMoreTokens()) {
            temp.append(tokenizerr.nextToken());
        }
        String last_degree = temp.toString();
        return last_degree;
    }

    public int check_price(String tweet) {
        String[] temps = tweet.split("[\\W]");
        for (String token : temps) {
            if (token.contains("charges") || token.contains("balance") || token.contains("load") || token.contains("price") || token.contains("Price") || token.contains("rate") || token.contains("pric")) {
                return 1;
            }
        }

        return 0;
    }

    public int check_3G4G(String tweet) {
        String[] temps = tweet.split("[\\W]");
        for (String token : temps) {
            if (token.contains("3G") || token.contains("4G") || token.contains("3g") || token.contains("4g")) {
                return 1;
            }
        }

        return 0;
    }

    public int check_network(String tweet) {
        String[] temps = tweet.split("[\\W]");
        for (String token : temps) {
            if (token.contains("Network") || token.contains("Signal") || token.contains("signal") || token.contains("connection") || token.contains("network") || token.contains("service")) {
                return 1;
            }

        }
        return 0;
    }

    public ArrayList cityStandardizer() throws FileNotFoundException, IOException {
        ArrayList<String> cities = new ArrayList<>();
        File file = new File("D:\\Practice\\Net beans\\SMF\\cities.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String read = " ";
        while ((read = br.readLine()) != null) {
            cities.add(read);
        }
        return cities;
    }

    public String checkCities(ArrayList<String> cities, String tweet) {
        String[] temps = tweet.split("\\W");
        for (String temp : temps) {
            for (String to : cities) {
                if (to.contains(temp)) {
                    return to;
                }
            }
        }
        ArrayList<String> cityName2 = new ArrayList<>();
        cityName2.add("Islamabad");
        cityName2.add("Lahore");
        cityName2.add("Karachi");
        cityName2.add("Rawalpindi");
        cityName2.add("Peshawar");
        cityName2.add("Quetta");
        cityName2.add("Faisalabad");
        cityName2.add("Multan");
        cityName2.add("Bahawalpur");
        cityName2.add("Gujranwala");
        cityName2.add("Dera Ghazi Khan");
        cityName2.add("Sialkot");
        cityName2.add("Hyderabad");

        SecureRandom r = new SecureRandom();
        int x = r.nextInt(13);
        return cityName2.get(x);
    }

    public String TranslateFeedback(String sentence) throws Exception {
        Translate.setClientId("socialinsight");
        Translate.setClientSecret("WsJlL0/fnSqi+UJs7k4YdA9EG/FTieqnChfPoa4QMpk=");
        sentence = Translate.execute(sentence, Language.ENGLISH);
//                 System.out.println(sentence);
        return sentence;
    }

    public ArrayList<String> readCity() throws FileNotFoundException, IOException {
        ArrayList<String> cities = new ArrayList<>();
        File file = new File("D:\\Practice\\Net beans\\SMF\\Lat Lon City.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String read = null;

        while ((read = br.readLine()) != null) {
            cities.add(read);
            //       System.out.println(read+"\n");
        }
        return cities;
    }
    
        public ArrayList<city> readLatLongCity() throws FileNotFoundException, IOException
        {
            ArrayList<city> cities=new ArrayList<city>();
            File file=new File("D:\\Practice\\Net beans\\SMF\\Lat Lon City.txt");
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            String read=" ";
            while((read=br.readLine())!=null)
            {
                city c=new city();
                String []temps=read.split(",");
                if(temps.length==3)
                {
                 c.setLatitude(Double.parseDouble(temps[0]));
                 c.setLongitude(Double.parseDouble(temps[1]));
                 c.setCityname(temps[2]);

                cities.add(c);
                }
             
            }
       return cities;       
}

}

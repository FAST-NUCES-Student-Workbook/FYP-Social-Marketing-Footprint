/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persitentstorage;

import Businesslogic.Feed;
import Businesslogic.Post;
import Businesslogic.tweet;
import Businesslogic.user;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

public class dbhandler {

    private static dbhandler instance;

    public dbhandler() {

    }

    public Connection gettConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String host = "jdbc:mysql://localhost:3306/smf?useUnicode=true&amp;characterEncoding=UTF-8&useServerPrepStmts=false&rewriteBatchedStatements=true&zeroDateTimeBehavior=convertToNull";
        String uName = "root";
        String uPass = "admin";
        Connection con = DriverManager.getConnection(host, uName, uPass);
        return con;
    }

    public static synchronized dbhandler getInstance() {
        if (instance == null) {
            instance = new dbhandler();
        }
        return instance;
    }

    public int adduser(user cu) throws SQLException, ClassNotFoundException {
        try {

            Connection con = this.gettConnection();
            java.sql.Statement stmt = con.createStatement();

            String sql = "Select * from smf.system_users";
            ResultSet res = stmt.executeQuery(sql);

            int n = 0;

            while (res.next()) {
                if (n < Integer.valueOf(res.getString("ID").substring(3))) {
                    n = Integer.valueOf(res.getString("ID").substring(3));
                }
            }

            n++;

            String ID = "SMF" + String.valueOf(n);

            String sql1 = "INSERT INTO smf.system_users(ID,NAME,PASSWORD,CNIC,ADDRESS,PHONE" + ") VALUES ('" + ID + "', '" + cu.getname() + "', '" + cu.getpassword() + "', '" + cu.getcnic() + "', '" + cu.getaddress() + "', '" + cu.getnumber() + "')";
            stmt.executeUpdate(sql1);
            return 1;
        } catch (SQLException e) {
            return 0;
        }
    }

    public ArrayList<user> populateUserList() throws SQLException, ClassNotFoundException {
        try {

            ArrayList<user> list = new ArrayList<>();

            Connection con = this.gettConnection();
            java.sql.Statement stmt = con.createStatement();
            String sql = "Select * from smf.system_users";
            ResultSet res = stmt.executeQuery(sql);

            while (res.next()) {
                String id = res.getString("ID");
                String name = res.getString("NAME");
                String password = res.getString("PASSWORD");
                String cnic = res.getString("CNIC");
                String phone = res.getString("PHONE");
                String address = res.getString("ADDRESS");

                user usr = new user(id, name, cnic, phone, address, password);

                list.add(usr);
            }
            System.out.println("populate");

            return list;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public int update(user cu) throws SQLException, ClassNotFoundException {
        try {
            System.out.println(cu.getid());
            Connection con = this.gettConnection();
            java.sql.Statement stmt = con.createStatement();
            String sql = "UPDATE smf.system_users SET NAME='" + cu.getname() + "', PASSWORD='" + cu.getpassword() + "',CNIC='" + cu.getcnic() + "', ADDRESS='" + cu.getaddress() + "', PHONE='" + cu.getnumber() + "' WHERE ID='" + cu.getid() + "'";
            stmt.executeUpdate(sql);
            System.out.println("Updated");
            return 1;
        } catch (SQLException e) {
            return 0;
        }
    }

    public int delete(user cu) throws ClassNotFoundException {

        System.out.println(cu.getid());
        System.out.println(cu.getpassword());
        try {
            Connection con = this.gettConnection();
            java.sql.Statement stmt = con.createStatement();
            String sql = "DELETE FROM smf.system_users WHERE ID='" + cu.getid() + "' AND PASSWORD='" + cu.getpassword() + "'";
            stmt.executeUpdate(sql);
            return 1;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        }
    }

    /////////////Insertion of extracted data from twitter//////////////////////////////////////
    public int addData(ArrayList<tweet> list) throws ClassNotFoundException, SQLException {

        Connection conn = this.gettConnection();
        //     conn.setAutoCommit(false);
        String sql1 = "INSERT INTO smf.twitter_data(tweet_id,TelcoName,text,user_name,favorited_count,retweeted_count,language,location,is_retweeted,created_time" + ") VALUES (?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql1);
        final int batchSize = 1000;
        int count = 0;
        for (tweet raw : list) {

            stmt.setString(1, raw.getTweet_id());
            stmt.setString(2, raw.getTelcoName());
            stmt.setString(3, raw.getText());
            stmt.setString(4, raw.getName());
            stmt.setInt(5, Integer.parseInt(raw.getFavorited_count()));
            stmt.setInt(6, Integer.parseInt(raw.getRetweeted_count()));
            stmt.setString(7, raw.getLanguage());
            stmt.setString(8, raw.getLocation());
            stmt.setString(9, Integer.toString(raw.getIs_retweeted()));
            stmt.setDate(10, (Date) raw.getCreated_date());
            //             System.out.println(raw.getTweet_id());
            stmt.addBatch();
            if (++count % batchSize == 0) {
                stmt.executeBatch();
            }

        }
        stmt.executeBatch();
        //     conn.close();
        System.out.println("System done with Db add data");
        return 0;

    }

    ///////The implementation of naive baysen with the help of lokkup table////////////////////////////  
    public void populate_stopping_words() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
        Connection conn = this.gettConnection();
        //    conn.setAutoCommit(false); 
        String inputLine;
        BufferedReader br = new BufferedReader(new FileReader(new File("stopping words.txt")));
        ArrayList<String> cty = new ArrayList<>();
        while ((inputLine = br.readLine()) != null) {

            cty.add(inputLine);
        }
        this.add_stopping_words(cty);
        //         conn.close();
    }

    ///////The implementation of naive baysen with the help of lokkup table////////////////////////////
    public void add_stopping_words(ArrayList<String> stri) throws ClassNotFoundException, SQLException {
        try (Connection conn = this.gettConnection()) {
            //      conn.setAutoCommit(false);
            String sql = "INSERT INTO smf.Stopping_words (word" + ") VALUES(?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            for (String st : stri) {
                stmt.setString(1, st);
                stmt.addBatch();
            }
            stmt.executeBatch();
            //      conn.close();
        }
    }

    ///////The implementation of naive baysen with the help of lokkup table////////////////////////////
    public void populate_positive_words() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
        Connection conn = this.gettConnection();
        //      conn.setAutoCommit(false); 
        String inputLine;
        BufferedReader br = new BufferedReader(new FileReader(new File("positive words.txt")));
        ArrayList<String> cty = new ArrayList<>();
        while ((inputLine = br.readLine()) != null) {

            cty.add(inputLine);
        }
        this.add_positive_words(cty);
        //          conn.close();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    public void add_positive_words(ArrayList<String> stri) throws ClassNotFoundException, SQLException {
        data_standardizer dbh = data_standardizer.getInstance();
        try (Connection conn = this.gettConnection()) {
            //    conn.setAutoCommit(false);
            String sql = "INSERT INTO smf.positive_words (word" + ") VALUES(?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            for (String st : stri) {
                ArrayList<String> lis = dbh.get_synonyms(st);
                for (String temp : lis) {
                    stmt.setString(1, temp);
                    stmt.addBatch();

                }
            }
            stmt.executeBatch();
            //       conn.close();
        }
    }

    ///////The implementation of naive baysen with the help of lokkup table////////////////////////////
    public void populate_negative_words() throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
        //         Connection conn=this.gettConnection();
        //        conn.setAutoCommit(false); 
        String inputLine;
        BufferedReader br = new BufferedReader(new FileReader(new File("negative words.txt")));
        ArrayList<String> cty = new ArrayList<>();
        while ((inputLine = br.readLine()) != null) {

            cty.add(inputLine);
        }
        this.add_negative_words(cty);
        //           conn.close();
    }

    ///////The implementation of naive baysen with the help of lokkup table////////////////////////////
    public void add_negative_words(ArrayList<String> stri) throws ClassNotFoundException, SQLException {
        data_standardizer dbh = data_standardizer.getInstance();
        // conn.setAutoCommit(false);
        try (Connection conn = this.gettConnection()) {
            // conn.setAutoCommit(false);
            String sql = "INSERT INTO smf.negative_words (word" + ") VALUES(?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            for (String st : stri) {
                ArrayList<String> lis = dbh.get_synonyms(st);
                for (String temp : lis) {
                    stmt.setString(1, temp);
                    stmt.addBatch();

                }
            }
            stmt.executeBatch();
            //        conn.close();
        }
    }

    ///////////Function for data profiling used to count distinct values of a column////////////////////////////
    public String count_distinct(String table, String column) throws ClassNotFoundException, SQLException {

        Connection conn = this.gettConnection();
        String sql = "SELECT COUNT(DISTINCT " + column + ") FROM " + table + "";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        rs.next();
        String result = rs.getString(1);
        //   conn.close();
        return result;
    }
///////Function used for data profiling to count null values of a specific column in atable ////////////////////////

    public String null_values(String table, String column) throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql = "SELECT COUNT(*) FROM " + table + " WHERE " + column + " IS NULL";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        rs.next();
        String result = rs.getString(1);
        //   conn.close();
        return result;
    }
///////////Function to standardize the location as well as KPI's for twitter ///////////////////////

    public void standard_language_location() throws ClassNotFoundException, SQLException, IOException, Exception {
        try (Connection conn = this.gettConnection()) {
            data_standardizer dbh = data_standardizer.getInstance();
            String sql1 = "SELECT * FROM smf.twitter_data";
            Statement stm = conn.createStatement();
            ResultSet data = stm.executeQuery(sql1);
            String batch = "INSERT INTO smf.twitter_transformed(tweet_id,telcoName,text,user_name,favorited_count,retweeted_count,language,location,is_retweeted,created_time,priceFactor,networkFactor,3G4G" + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(batch);
            ArrayList<String> cityName = dbh.cityStandardizer();
            while (data.next()) {
                stmt.setString(1, data.getString(1));
                stmt.setString(2, data.getString(2));
                String txt = dbh.set_text(data.getString(3));   
                stmt.setString(3, txt);                       // add without translation
//               stmt.setString(3, dbh.TranslateFeedback(txt));      // uncomment this to Translate the sentence
                stmt.setString(4, data.getString(4));
                stmt.setInt(5, data.getInt(5));
                stmt.setInt(6, data.getInt(6));
                stmt.setString(7, dbh.set_language(data.getString(7)));
                stmt.setString(8, dbh.set_location(data.getString(8), cityName));
                stmt.setInt(9, data.getInt(9));
                stmt.setDate(10, data.getDate(10));
                stmt.setInt(11, dbh.check_price(txt));
                stmt.setInt(12, dbh.check_network(txt));
                stmt.setInt(13, dbh.check_3G4G(txt));
                stmt.addBatch();

            }
            stmt.executeBatch();
            //      conn.close();
        }
    }

    ///////////Function to standardize the location as well as KPI's for facebook   ///////////////////////
    public void standardLocationFB() throws ClassNotFoundException, SQLException, IOException, Exception {
        {
            try (Connection conn = this.gettConnection()) {
                data_standardizer dbh = data_standardizer.getInstance();
                ArrayList<String> cityName = dbh.cityStandardizer();
                String sql1 = "SELECT * FROM smf.feed";
                Statement stm = conn.createStatement();
                ResultSet data = stm.executeQuery(sql1);
                String batch = "INSERT INTO smf.feed_transformed(pid,commentText,commentLikes,location,priceFactor,networkFactor,3G4G" + ") VALUES (?,?,?,?,?,?,?)";
                PreparedStatement stmt = conn.prepareStatement(batch);

                while (data.next()) {
                    stmt.setString(1, data.getString(1));
                    String txt = dbh.set_text(data.getString(2));
 //               stmt.setString(2, dbh.TranslateFeedback(txt));   // uncomment this to Translate the sentence

                    stmt.setString(2, txt);
                    stmt.setInt(3, data.getInt(3));

                    stmt.setString(4, dbh.checkCities(cityName, txt));
                    stmt.setInt(5, dbh.check_price(txt));
                    stmt.setInt(6, dbh.check_network(txt));
                    stmt.setInt(7, dbh.check_3G4G(txt));
                    stmt.addBatch();
                }
                stmt.executeBatch();
                //      conn.close();
            }
        }
    }

///////////////////  Twitter Sentimental Analysis //////////////////////////
    public void semantic_Analysis() throws ClassNotFoundException, SQLException, IOException {
        data_standardizer dbh = data_standardizer.getInstance();
        Connection conn = this.gettConnection();
        String sql1 = "SELECT * FROM smf.twitter_TRANSFORMED";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.twitter_semantic(tweet_id,telcoName,text,favoritedCount,retweetedCount,location,created_time,semantic,priceFactor,networkFactor,3G4G" + ") VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            double result = dbh.calculate_strength(data.getString(3));
            stmt.setString(1, data.getString(1));
            stmt.setString(2, data.getString(2));
            stmt.setString(3, data.getString(3));
            stmt.setInt(4, data.getInt(5));
            stmt.setInt(5, data.getInt(6));
            stmt.setString(6, data.getString(8));
            stmt.setDate(7, data.getDate(10));
            stmt.setDouble(8, result);
            stmt.setInt(9, data.getInt(11));
            stmt.setInt(10, data.getInt(12));
            stmt.setInt(11, data.getInt(13));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();

    }

    ///////////////////  Facebook Sentimental Analysis //////////////////////////
    public void semantic_AnalysisFB() throws ClassNotFoundException, SQLException, IOException {
        data_standardizer dbh = data_standardizer.getInstance();
        Connection conn = this.gettConnection();
        String sql1 = "SELECT * FROM smf.feed_transformed";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.feed_semantic(PID,commentText,location,semantic,priceFactor,networkFactor,3G4G) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            double result = (dbh.calculate_strength(data.getString(2)));
            // effectiveness of number of likes should be here
            stmt.setString(1, data.getString(1));
            stmt.setString(2, data.getString(2));
            stmt.setString(3, data.getString(4));
            stmt.setDouble(4, result);
            stmt.setInt(5, data.getInt(5));
            stmt.setInt(6, data.getInt(6));
            stmt.setInt(7, data.getInt(7));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
    }

    public void semanticPostFB() throws ClassNotFoundException, SQLException, IOException {
//        data_standardizer dbh = data_standardizer.getInstance();
        Connection conn = this.gettConnection();
        System.out.println("feed semantic populated going 4 post semantic");
        String sql3 = "SELECT * FROM smf.post";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql3);
        String batch1 = "INSERT INTO smf.post_semantic(PID,telcoName,postText,likeCount,sharesCount,CreatedTime,semantic" + ") VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(batch1);
        while (data2.next()) {
            Double semanticSum = 0.0;

            String sql2 = "SELECT semantic FROM smf.feed_semantic where PID='" + data2.getString(1) + "'";
            Statement stm1 = conn.createStatement();
            ResultSet data1 = stm1.executeQuery(sql2);

            while (data1.next()) {
                semanticSum += Double.parseDouble(data1.getString(1));
            }
            stmt1.setString(1, data2.getString(1));
            stmt1.setString(2, data2.getString(2));
            stmt1.setString(3, data2.getString(3));
            stmt1.setInt(4, data2.getInt(5));
            stmt1.setInt(5, data2.getInt(4));
            stmt1.setDate(6, data2.getDate(7));
            int divider = data2.getInt(6);
            if (divider >= 1) {
                stmt1.setDouble(7, (semanticSum / divider));
            } else {
                stmt1.setDouble(7, (semanticSum));
            }
            stmt1.addBatch();
        }
        stmt1.executeBatch();
        System.out.println("post semantic populated");
    }
//  public String remove_stopping_words(String sentence) throws ClassNotFoundException, SQLException
// {
//   ArrayList<String>list=new ArrayList<>();
//   String []token=sentence.split(" ");
//   list.addAll(Arrays.asList(token));
//   String result;
//     try (Connection conn = this.gettConnection()) {
//         for(int i=0;i<list.size();i++)
//         {
//             String sql="select distinct(word) from smf.stopping_words where stopping_words.word='"+list.get(i)+"'";
//             Statement stm =conn.createStatement();
//             ResultSet rs=stm.executeQuery(sql);
//             if(rs.next())
//             {
//                 list.remove(i);
//             }
//         }     result = " ";
//   for(String st:list)
//   {
//       result=result+st+" ";
//   }
////   conn.close();
//     }
//   return result;
// }
//    
    //////////////////////////////// Facebook Data Extraction /////////////////////////

    public void addnewPosts(ArrayList<Post> plist) throws ClassNotFoundException, SQLException {
        data_standardizer dbh = data_standardizer.getInstance();
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.post(PID,TelcoName, PostText, SharesCount, LikeCount, NofComments, CreatedTime) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql1);

        final int batchSize = plist.size();
        int count = 0;

        for (Post reg : plist) {
            stmt.setString(1, reg.getPostID());
            stmt.setString(2, reg.getTelcoName());
            stmt.setString(3, dbh.set_text(reg.getPostText()));
            stmt.setInt(4, reg.getSharesCount());
            stmt.setInt(5, reg.getLikeCounts());
            stmt.setInt(6, reg.getNofComments());
            stmt.setDate(7, reg.getCreatedTime());
            stmt.addBatch();

            if (++count % batchSize == 0) {
                stmt.executeBatch();
            }
        }
        try {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
        System.out.println("System done with Db add data");
    }

    public void addnewFeeds(ArrayList<Feed> flist) throws ClassNotFoundException, SQLException {

        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.feed(PID, commentText, commentLikes) VALUES (?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql1);

        final int batchSize = flist.size();
        int count = 0;

        for (Feed reg : flist) {
            stmt.setString(1, reg.getPostID());
            stmt.setString(2, reg.getCommentText());
            stmt.setInt(3, reg.getCommentLikes());
            stmt.addBatch();

            if (++count % batchSize == 0) {
                stmt.executeBatch();
            }
        }
        try {
            stmt.executeBatch();

        } catch (BatchUpdateException e) {
            //you should handle exception for failed records here
            System.out.println(e.getMessage());
        }
    }

    public void addLocationDimention() throws ClassNotFoundException, SQLException, IOException {
        data_standardizer dbh = data_standardizer.getInstance();
        ArrayList<city> cities = dbh.readLatLongCity();
        Connection conn = this.gettConnection();
        String sql = "INSERT INTO smf.dimension_location (locId,locName,locLongitude,locLatitude) VALUES(?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        int counter = 0;
        for (city c : cities) {
            counter++;
            stmt.setString(1, "L" + counter);
            stmt.setString(2, c.getCityname());
            stmt.setDouble(3, c.getLongitude());
            stmt.setDouble(4, c.getLatitude());
//         stmt.setFloat(4,Float.parseFloat(tokens[1]));
            stmt.addBatch();
        }
        stmt.executeBatch();
    }

    public void addTelcoDimension() throws ClassNotFoundException, SQLException {
        ArrayList<String> dlist = new ArrayList<>();
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.dimension_telco(telcoID,telcoName) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct TelcoName FROM smf.post_semantic";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            dlist.add(data2.getString(1));
        }
        String sql3 = "SELECT distinct TelcoName FROM smf.twitter_semantic";
        Statement stm3 = conn.createStatement();
        ResultSet data3 = stm3.executeQuery(sql3);

        while (data3.next()) {
            if (!dlist.contains(data3.getString(1))) {
                dlist.add(data3.getString(1));
            }

        }
        for (String d : dlist) {
            stmt1.setString(1, d);
            switch (d) {
                case "M1":
                    stmt1.setString(2, "Mobilink");
                    break;
                case "T2":
                    stmt1.setString(2, "Telenor");
                    break;
                case "U3":
                    stmt1.setString(2, "Ufone");
                    break;
                case "Z4":
                    stmt1.setString(2, "Zong");
                    break;
                case "W5":
                    stmt1.setString(2, "Warid");
                    break;
            }
            stmt1.addBatch();
        }
        stmt1.executeBatch();
    }

    public void addTimeDimension() throws ClassNotFoundException, SQLException {
        ArrayList<Date> dlist = new ArrayList<>();
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.dimension_time(timeID,date,timeDay,timeMonth,timeYear) VALUES (?,?,?,?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct CreatedTime FROM smf.post_semantic";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            dlist.add(data2.getDate(1));
        }
        String sql3 = "SELECT distinct Created_Time FROM smf.twitter_semantic";
        Statement stm3 = conn.createStatement();
        ResultSet data3 = stm3.executeQuery(sql3);

        while (data3.next()) {
            if (!dlist.contains(data3.getDate(1))) {
                dlist.add(data3.getDate(1));
            }

        }
        int count = 0;
        for (Date d : dlist) {
            count++;
            stmt1.setString(1, "T" + count);
            stmt1.setDate(2, d);
            stmt1.setInt(3, d.getDate());
            stmt1.setInt(4, d.getMonth() + 1);
            stmt1.setInt(5, d.getYear() + 1900);
            stmt1.addBatch();
        }
        stmt1.executeBatch();
    }

    public void addCommentDimension() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.dimension_comment(postID,commentText) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT  PID,commentText FROM smf.feed_semantic";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            stmt1.setString(1, data2.getString(1));
            stmt1.setString(2, data2.getString(2));
            stmt1.addBatch();
        }
        stmt1.executeBatch();
    }

    public void addPostDimension() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.dimension_post(postID,postText,likesCount,sharesCount) VALUES (?,?,?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT  PID,PostText,likeCount,sharesCount FROM smf.post_semantic";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        String sql3 = "SELECT  tweet_id,text,favoritedCount,retweetedCount FROM smf.twitter_semantic";
        Statement stm3 = conn.createStatement();
        ResultSet data3 = stm3.executeQuery(sql3);
        while (data2.next()) {
            stmt1.setString(1, data2.getString(1));
            stmt1.setString(2, data2.getString(2));
            stmt1.setString(3, data2.getString(3));
            stmt1.setString(4, data2.getString(4));
            stmt1.addBatch();
        }
        while (data3.next()) {
            stmt1.setString(1, data3.getString(1));
            stmt1.setString(2, data3.getString(2));
            stmt1.setString(3, data3.getString(3));
            stmt1.setString(4, data3.getString(4));
            stmt1.addBatch();
        }
        stmt1.executeBatch();
    }

    public void addFactTable() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.fact_table(postID,telcoID,timeID,locID,semantic,commentSemantic,priceFactor,networkFactor,3G4G) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT tweet_id,TelcoName,created_time,location,semantic,priceFactor,networkFactor,3G4G FROM smf.twitter_semantic";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);

        while (data2.next()) {
            stmt1.setString(1, data2.getString(1));
            stmt1.setString(2, data2.getString(2));
            String sql3 = "SELECT  timeID FROM smf.dimension_time where '" + data2.getDate(3) + "' = dimension_time.date";
            Statement stm3 = conn.createStatement();
            ResultSet data3 = stm3.executeQuery(sql3);
            //           while (data3.next()) {
            //             System.out.println(data3.getString(1));
            data3.next();
            stmt1.setString(3, data3.getString(1));
//            }
//            System.out.println(data2.getString(3));
            String sql4 = "SELECT  locID FROM smf.dimension_location where dimension_location.locName = '" + data2.getString(4) + "' ";
            Statement stm4 = conn.createStatement();
            ResultSet data4 = stm4.executeQuery(sql4);
            data4.next();
//            System.out.println(data4.getString(1));
            stmt1.setString(4, data4.getString(1));
            stmt1.setString(5, data2.getString(5));
            stmt1.setString(6, data2.getString(5));
            stmt1.setInt(7, data2.getInt(6));
            stmt1.setInt(8, data2.getInt(7));
            stmt1.setInt(9, data2.getInt(8));
            stmt1.addBatch();
        }
        // adding data from feed_semamtic table    
        String sql5 = "insert into smf.fact_table(postID,telcoID,timeID,locID,semantic,commentSemantic,priceFactor,networkFactor,3G4G) VALUES (?,?,?,?,?,?,?,?,?)";
        PreparedStatement stmt5 = conn.prepareStatement(sql5);
        String sql6 = "SELECT PID,location,semantic,priceFactor,networkFactor,3G4G FROM smf.feed_semantic";
        Statement stm6 = conn.createStatement();
        ResultSet data6 = stm6.executeQuery(sql6);

        while (data6.next()) {
            stmt1.setString(1, data6.getString(1));
            String sql7 = "SELECT  TelcoName,Createdtime,semantic FROM smf.post_semantic where '" + data6.getString(1) + "' = post_semantic.PID";
            Statement stm7 = conn.createStatement();
            ResultSet data7 = stm7.executeQuery(sql7);
            data7.next();
            stmt1.setString(2, data7.getString(1));

            String sql8 = "SELECT  timeID FROM smf.dimension_time where '" + data7.getString(2) + "' = dimension_time.date";
            Statement stm8 = conn.createStatement();
            ResultSet data8 = stm8.executeQuery(sql8);
            data8.next();
            stmt1.setString(3, data8.getString(1));

            String sql9 = "SELECT locID FROM smf.dimension_location where dimension_location.locName = '" + data6.getString(2) + "' ";
            Statement stm9 = conn.createStatement();
            ResultSet data9 = stm9.executeQuery(sql9);
            data9.next();
//            System.out.println(data4.getString(1));
            stmt1.setString(4, data9.getString(1));
            stmt1.setString(5, data7.getString(3));
            stmt1.setString(6, data6.getString(3));
            stmt1.setInt(7, data6.getInt(4));
            stmt1.setInt(8, data6.getInt(5));
            stmt1.setInt(9, data6.getInt(6));
            stmt1.addBatch();
        }
        stmt1.executeBatch();
    }

    ////////////////////////////////////////////////////////////////
    //******************  BUSINESS QUESTIONS *********************//
    ////////////////////////////////////////////////////////////////
    public void bqServiceSatisfaction() throws ClassNotFoundException, SQLException {
        int z = 0, t = 0, u = 0, w = 0, m = 0;
        double zz = 0.0, tt = 0.0, uu = 0.0, ww = 0.0, mm = 0.0;
        String telcoName = null;
        Connection conn = this.gettConnection();
        String sql1 = "insert into bq_service_satisfaction(telcoName,ratings) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID`";
        //SELECT telcoId,Avg(semantic) FROM fact_table Group By telcoID; //in this query it is taking avg with overall count
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            telcoName = data2.getString(2);
            switch (telcoName) {
                case "M1":
                    m++;
                    mm = mm + data2.getDouble(3);
                    break;
                case "T2":
                    t++;
                    tt = tt + data2.getDouble(3);
                    break;
                case "U3":
                    u++;
                    uu = uu + data2.getDouble(3);
                    break;
                case "Z4":
                    z++;
                    zz = zz + data2.getDouble(3);
                    break;
                case "W5":
                    w++;
                    ww = ww + data2.getDouble(3);
                    break;
            }
        }

        stmt1.setString(1, "Mobilink");
        stmt1.setDouble(2, mm / m);
        stmt1.addBatch();        // Adding for mobilink

        stmt1.setString(1, "Telenor");
        stmt1.setDouble(2, tt / t);
        stmt1.addBatch();        // Adding for telenor
        stmt1.setString(1, "Ufone");
        stmt1.setDouble(2, uu / u);
        stmt1.addBatch();        // Adding for Ufone
        stmt1.setString(1, "Zong");
        stmt1.setDouble(2, zz / z);
        stmt1.addBatch();        // Adding for Zong
        stmt1.setString(1, "Warid");
        stmt1.setDouble(2, ww / w);
        stmt1.addBatch();        // Adding for Warid
        stmt1.executeBatch();
    }

    public void bqSocialMediaTraffic() throws ClassNotFoundException, SQLException {

        double z = 1, t = 1, u = 1, w = 1, m = 1;     // counter of total number of records occur
        double t_z = 0, t_t = 0, t_u = 0, t_w = 0, t_m = 0;
        double zz = 0, tt = 0, uu = 0, ww = 0, mm = 0;  // total number of likes done
        double t_zz = 0, t_tt = 0, t_uu = 0, t_ww = 0, t_mm = 0;
        double zzz = 0, ttt = 0, uuu = 0, www = 0, mmm = 0;   // total number of shares count
        double t_zzz = 0, t_ttt = 0, t_uuu = 0, t_www = 0, t_mmm = 0;

        Connection conn = this.gettConnection();
        String sql1 = "insert into smf.bq_social_media_traffic(socialMedia,telcoName,AvgLikeCount,AvgShareCount) VALUES (?,?,?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT  distinct(a.postID),a.telcoID FROM smf.fact_table a,smf.dimension_post b where a.`postID` = b.`postID`";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            String pid = data2.getString(1);
            String telconame = data2.getString(2);
            switch (telconame) {
                case "M1":
                    if (pid.startsWith("P")) {
                        m++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID =  '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        mm += data3.getInt(1);
                        mmm += data3.getInt(2);
                    } else if (pid.startsWith("t")) {
                        t_m++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        t_mm += data3.getInt(1);
                        t_mmm += data3.getInt(2);
                    }
                    break;
                case "T2":
                    if (pid.startsWith("P")) {
                        t++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        tt += data3.getInt(1);
                        ttt += data3.getInt(2);
                    } else if (pid.startsWith("t")) {
                        t_t++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        t_tt += data3.getInt(1);
                        t_ttt += data3.getInt(2);
                    }
                    break;
                case "U3":
                    if (pid.startsWith("P")) {
                        u++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        uu += data3.getInt(1);
                        uuu += data3.getInt(2);
                    } else if (pid.startsWith("t")) {
                        t_u++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        t_uu += data3.getInt(1);
                        t_uuu += data3.getInt(2);
                    }
                    break;
                case "Z4":
                    if (pid.startsWith("P")) {
                        z++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        zz += data3.getInt(1);
                        zzz += data3.getInt(2);
                    } else if (pid.startsWith("t")) {
                        t_z++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        t_zz += data3.getInt(1);
                        t_zzz += data3.getInt(2);
                    }
                    break;
                case "W5":
                    if (pid.startsWith("P")) {
                        w++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID =  '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        ww += data3.getInt(1);
                        www += data3.getInt(2);
                    } else if (pid.startsWith("t")) {
                        t_w++;
                        String sql3 = "SELECT likesCount,sharesCount FROM smf.dimension_post where postID = '" + pid + "'";
                        Statement stm3 = conn.createStatement();
                        ResultSet data3 = stm3.executeQuery(sql3);
                        data3.next();
                        t_ww += data3.getInt(1);
                        t_www += data3.getInt(2);
                    }
                    break;
            }
        }

//        System.out.println("FB:Telenor counter: "+t+" likes total: "+tt+" share total: "+ttt+" avglikes:"+tt/t+" avg: "+ttt/t);
//        System.out.println("TW:Telenor counter: "+t_t+" likes total: "+t_tt+" share total: "+t_ttt+" avglikes:"+t_tt/t_t+" avg: "+t_ttt/t_t);
//        System.out.println("FB:Mobilink counter: "+m+" likes total: "+mm+" share total: "+mmm+" avglikes:"+mm/m+" avg: "+mmm/m);
//        System.out.println("TW:Mobilink counter: "+t_m+" likes total: "+t_mm+" share total: "+t_mmm+" avglikes:"+t_mm/t_m+" avg: "+t_mmm/t_m);
//        System.out.println("FB:Ufone counter: "+u+" likes total: "+uu+" share total: "+uuu+" avglikes:"+uu/u+" avg: "+uuu/u);
//        System.out.println("TW:Ufone counter: "+t_u+" likes total: "+t_uu+" share total: "+t_uuu+" avglikes:"+t_uu/t_u+" avg: "+t_uuu/t_u);
        stmt1.setString(1, "Facebook");
        stmt1.setString(2, "Telenor");
        stmt1.setDouble(3, tt / t);
        stmt1.setDouble(4, ttt / t);
        stmt1.addBatch();              // Adding for telenor 
        stmt1.setString(1, "Facebook");
        stmt1.setString(2, "Mobilink");
        stmt1.setDouble(3, mm / m);
        stmt1.setDouble(4, mmm / m);
        stmt1.addBatch();              // Adding for mobilink
        stmt1.setString(1, "Facebook");
        stmt1.setString(2, "Ufone");
        stmt1.setDouble(3, uu / u);
        stmt1.setDouble(4, uuu / u);
        stmt1.addBatch();              // Adding for ufone
        stmt1.setString(1, "Facebook");
        stmt1.setString(2, "Zong");
        stmt1.setDouble(3, zz / z);
        stmt1.setDouble(4, zzz / z);
        stmt1.addBatch();              // Adding for zong
        stmt1.setString(1, "Facebook");
        stmt1.setString(2, "Warid");
        stmt1.setDouble(3, ww / w);
        stmt1.setDouble(4, www / w);
        stmt1.addBatch();              // Adding for warid

        stmt1.setString(1, "Twitter");
        stmt1.setString(2, "Telenor");
        stmt1.setDouble(3, t_tt / t_t);
        stmt1.setDouble(4, t_ttt / t_t);
        stmt1.addBatch();              // Adding for telenor 
        stmt1.setString(1, "Twitter");
        stmt1.setString(2, "Mobilink");
        stmt1.setDouble(3, t_mm / t_m);
        stmt1.setDouble(4, t_mmm / t_m);
        stmt1.addBatch();              // Adding for mobilink
        stmt1.setString(1, "Twitter");
        stmt1.setString(2, "Ufone");
        stmt1.setDouble(3, t_uu / t_u);
        stmt1.setDouble(4, t_uuu / t_u);
        stmt1.addBatch();              // Adding for ufone
        stmt1.setString(1, "Twitter");
        stmt1.setString(2, "Zong");
        stmt1.setDouble(3, t_zz / t_z);
        stmt1.setDouble(4, t_zzz / t_z);
        stmt1.addBatch();              // Adding for zong
        stmt1.setString(1, "Twitter");
        stmt1.setString(2, "Warid");
        stmt1.setDouble(3, t_ww / t_w);
        stmt1.setDouble(4, t_www / t_w);
        stmt1.addBatch();              // Adding for warid        
        stmt1.executeBatch();
    }

    public void bqCompetitiveadvantagePrice() throws ClassNotFoundException, SQLException {
        int z = 0, t = 0, u = 0, w = 0, m = 0;
        double zz = 0.0, tt = 0.0, uu = 0.0, ww = 0.0, mm = 0.0;
        String telcoName = null;
        Connection conn = this.gettConnection();
        String sql1 = "insert into bq_competitive_advantage_price(telcoName,rating) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.pricefactor='1'";
        //SELECT telcoId,Avg(semantic) FROM fact_table Group By telcoID; //in this query it is taking avg with overall count
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            telcoName = data2.getString(2);
            switch (telcoName) {
                case "M1":
                    m++;
                    mm = mm + data2.getDouble(3);
                    break;
                case "T2":
                    t++;
                    tt = tt + data2.getDouble(3);
                    break;
                case "U3":
                    u++;
                    uu = uu + data2.getDouble(3);
                    break;
                case "Z4":
                    z++;
                    zz = zz + data2.getDouble(3);
                    break;
                case "W5":
                    w++;
                    ww = ww + data2.getDouble(3);
                    break;
            }
        }

        stmt1.setString(1, "Mobilink");
        stmt1.setDouble(2, mm / m);
        stmt1.addBatch();        // Adding for mobilink

        stmt1.setString(1, "Telenor");
        stmt1.setDouble(2, tt / t);
        stmt1.addBatch();        // Adding for telenor
        stmt1.setString(1, "Ufone");
        stmt1.setDouble(2, uu / u);
        stmt1.addBatch();        // Adding for Ufone
        stmt1.setString(1, "Zong");
        stmt1.setDouble(2, zz / z);
        stmt1.addBatch();        // Adding for Zong
        stmt1.setString(1, "Warid");
        stmt1.setDouble(2, ww / w);
        stmt1.addBatch();        // Adding for Warid
        stmt1.executeBatch();
    }

    public void bqCompetitiveadvantageNetwork() throws ClassNotFoundException, SQLException {
        int z = 0, t = 0, u = 0, w = 0, m = 0;
        double zz = 0.0, tt = 0.0, uu = 0.0, ww = 0.0, mm = 0.0;
        String telcoName = null;
        Connection conn = this.gettConnection();
        String sql1 = "insert into bq_competitive_advantage_network(telcoName,rating) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.networkfactor='1'";
        //SELECT telcoId,Avg(semantic) FROM fact_table Group By telcoID; //in this query it is taking avg with overall count
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            telcoName = data2.getString(2);
            switch (telcoName) {
                case "M1":
                    m++;
                    mm = mm + data2.getDouble(3);
                    break;
                case "T2":
                    t++;
                    tt = tt + data2.getDouble(3);
                    break;
                case "U3":
                    u++;
                    uu = uu + data2.getDouble(3);
                    break;
                case "Z4":
                    z++;
                    zz = zz + data2.getDouble(3);
                    break;
                case "W5":
                    w++;
                    ww = ww + data2.getDouble(3);
                    break;
            }
        }

        stmt1.setString(1, "Mobilink");
        stmt1.setDouble(2, mm / m);
        stmt1.addBatch();        // Adding for mobilink

        stmt1.setString(1, "Telenor");
        stmt1.setDouble(2, tt / t);
        stmt1.addBatch();        // Adding for telenor
        stmt1.setString(1, "Ufone");
        stmt1.setDouble(2, uu / u);
        stmt1.addBatch();        // Adding for Ufone
        stmt1.setString(1, "Zong");
        stmt1.setDouble(2, zz / z);
        stmt1.addBatch();        // Adding for Zong
        stmt1.setString(1, "Warid");
        stmt1.setDouble(2, ww / w);
        stmt1.addBatch();        // Adding for Warid
        stmt1.executeBatch();
    }

    public void bqCompetitiveadvantage3G4G() throws ClassNotFoundException, SQLException {
        int z = 0, t = 0, u = 0, w = 0, m = 0;
        double zz = 0.0, tt = 0.0, uu = 0.0, ww = 0.0, mm = 0.0;
        String telcoName = null;
        Connection conn = this.gettConnection();
        String sql1 = "insert into bq_competitive_advantage_3g4g(telcoName,rating) VALUES (?,?)";
        PreparedStatement stmt1 = conn.prepareStatement(sql1);
        String sql2 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.3G4G='1'";
        //SELECT telcoId,Avg(semantic) FROM fact_table Group By telcoID; //in this query it is taking avg with overall count
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while (data2.next()) {
            telcoName = data2.getString(2);
            switch (telcoName) {
                case "M1":
                    m++;
                    mm = mm + data2.getDouble(3);
                    break;
                case "T2":
                    t++;
                    tt = tt + data2.getDouble(3);
                    break;
                case "U3":
                    u++;
                    uu = uu + data2.getDouble(3);
                    break;
                case "Z4":
                    z++;
                    zz = zz + data2.getDouble(3);
                    break;
                case "W5":
                    w++;
                    ww = ww + data2.getDouble(3);
                    break;
            }
        }

        stmt1.setString(1, "Mobilink");
        stmt1.setDouble(2, mm / m);
        stmt1.addBatch();        // Adding for mobilink

        stmt1.setString(1, "Telenor");
        stmt1.setDouble(2, tt / t);
        stmt1.addBatch();        // Adding for telenor
        stmt1.setString(1, "Ufone");
        stmt1.setDouble(2, uu / u);
        stmt1.addBatch();        // Adding for Ufone
        stmt1.setString(1, "Zong");
        stmt1.setDouble(2, zz / z);
        stmt1.addBatch();        // Adding for Zong
        stmt1.setString(1, "Warid");
        stmt1.setDouble(2, ww / w);
        stmt1.addBatch();        // Adding for Warid
        stmt1.executeBatch();
    }

    public void bq_bigger_network() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "select T.telcoName,L.locName,L.locLongitude,L.locLatitude,count(L.locName) as Users from fact_table as F, dimension_location as L, dimension_telco as T where F.telcoID= T.telcoID AND F.locID = L.locID group by T.telcoName,L.locName;";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.bq_bigger_network(telcoName,locName,locLongitude,locLatitude,users" + ") VALUES (?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            stmt.setString(1, data.getString(1));

            stmt.setString(2, data.getString(2));
            stmt.setDouble(3, data.getDouble(3));
            stmt.setDouble(4, data.getDouble(4));
            stmt.setInt(5, data.getInt(5));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
    }

    public void bq_geographically_dominating() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "select T.telcoName,L.locName,L.locLongitude,L.locLatitude,count(L.locName) as Users from fact_table as F, dimension_location as L, dimension_telco as T where F.telcoID= T.telcoID AND F.locID = L.locID group by T.telcoName,L.locName;";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.bq_geographically_dominating(telcoName,locName,locLongitude,locLatitude,users" + ") VALUES (?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            stmt.setString(1, data.getString(1));

            stmt.setString(2, data.getString(2));
            stmt.setDouble(3, data.getDouble(3));
            stmt.setDouble(4, data.getDouble(4));
            stmt.setInt(5, data.getInt(5));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
    }

    public void bq_monthly_trend() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "select T.date,T.timeMonth,T.timeDay,C.telcoName, '1' + Sum(F.semantic)%5 as Ratings from fact_table as F,dimension_time as T,dimension_telco as C where F.telcoID=C.telcoID AND F.timeID=T.timeID group by T.timeMonth,T.timeDay,C.telcoName;";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.bq_monthly_trend(date,timeMonth,timeDay,telcoName,ratings" + ") VALUES (?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            stmt.setString(1, data.getString(1));
            stmt.setInt(2, data.getInt(2));
            stmt.setInt(3, data.getInt(3));
            stmt.setString(4, data.getString(4));
            stmt.setDouble(5, data.getDouble(5));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
    }

    public void bq_monthly_ratings() throws ClassNotFoundException, SQLException {
        Connection conn = this.gettConnection();
        String sql1 = "select T.date,C.telcoName,Sum(F.semantic)%5 as Ratings from fact_table as F,dimension_time as T,dimension_telco as C where F.telcoID=C.telcoID AND F.timeID=T.timeID group by t.date, C.telcoName;";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.bq_monthly_ratings(date,telcoName,ratings" + ") VALUES (?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            stmt.setDate(1, data.getDate(1));
            stmt.setString(2, data.getString(2));
            stmt.setDouble(3, data.getDouble(3));
             stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
    }
    public void bqFactorDistribution() throws ClassNotFoundException, SQLException
            
    {
        int m1 = 0,m2 = 0,m3=0,m4=0,m5=0,m6=0;
        int w1 = 0,w2 = 0,w3=0,w4=0,w5=0,w6=0;
        int t1 = 0,t2 = 0,t3=0,t4=0,t5=0,t6=0;
        int u1 = 0,u2 = 0,u3=0,u4=0,u5=0,u6=0;
        int z1 = 0,z2 = 0,z3=0,z4=0,z5=0,z6=0;
        
        Connection conn=this.gettConnection();
        String telcoName = null;
        String sql1 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.3G4G='1' AND a.semantic<2.5";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        
        while(data.next())
        {
           
           telcoName = data.getString(2);
            switch (telcoName) 
            {
                case "M1":
                   m1++;
                    break;
                case "T2":
                   t1++;
                    break;
                case "U3":
                    u1++;
                    break;
                case "Z4":
                    z1++;
                    break;
                case "W5":
                   w1++;
                    break;
            }}
        String sql2 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.networkFactor='1' AND a.semantic<2.5";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        while(data2.next())
        {
           
           telcoName = data2.getString(2);
            switch (telcoName) {
                case "M1":
                   m2++;
                    break;
                case "T2":
                   t2++;
                    break;
                case "U3":
                    u2++;
                    break;
                case "Z4":
                    z2++;
                    break;
                case "W5":
                   w2++;
                    break;
            }}
        String sql3 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.priceFactor='1' AND a.semantic<2.5";
        Statement stm3 = conn.createStatement();
        ResultSet data3 = stm3.executeQuery(sql3);
               while(data3.next())
        {
           
           telcoName = data3.getString(2);
            switch (telcoName) {
                case "M1":
                   m3++;
                    break;
                case "T2":
                   t3++;
                    break;
                case "U3":
                    u3++;
                    break;
                case "Z4":
                    z3++;
                    break;
                case "W5":
                   w3++;
                    break;
            }
        }
       /////////////////////////////////////////////////////////////////////////////////////////////////////
       String sql4 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.3G4G='1' AND a.semantic>2.5";
        Statement stm4 = conn.createStatement();
        ResultSet data4 = stm4.executeQuery(sql4);
        
        while(data4.next())
        {
           
           telcoName = data4.getString(2);
            switch (telcoName) 
            {
                case "M1":
                   m4++;
                    break;
                case "T2":
                   t4++;
                    break;
                case "U3":
                    u4++;
                    break;
                case "Z4":
                    z4++;
                    break;
                case "W5":
                   w4++;
                    break;
            }}
        String sql5 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.networkFactor='1' AND a.semantic>2.5";
        Statement stm5 = conn.createStatement();
        ResultSet data5 = stm5.executeQuery(sql5);
        while(data5.next())
        {
           
           telcoName = data5.getString(2);
            switch (telcoName) {
                case "M1":
                   m5++;
                    break;
                case "T2":
                   t5++;
                    break;
                case "U3":
                    u5++;
                    break;
                case "Z4":
                    z5++;
                    break;
                case "W5":
                   w5++;
                    break;
            }}
        String sql6 = "SELECT distinct(a.postID),a.telcoId,a.semantic FROM fact_table a,dimension_post b,dimension_telco c where a.`postID`=b.`postID` AND a.`telcoID`=c.`telcoID` AND a.priceFactor='1' AND a.semantic>2.5";
        Statement stm6 = conn.createStatement();
        ResultSet data6 = stm6.executeQuery(sql6);
               while(data6.next())
        {
           
           telcoName = data6.getString(2);
            switch (telcoName) {
                case "M1":
                   m6++;
                    break;
                case "T2":
                   t6++;
                    break;
                case "U3":
                    u6++;
                    break;
                case "Z4":
                    z6++;
                    break;
                case "W5":
                   w6++;
                    break;
            }
        }
   
       ////////////////////////////////////////////////////////////////////////////////////////////////////
    String sql = "insert into bq_factor_distribution(telcoName,npriceUsers,nnetworkUsers,n3g4gUsers,ppriceUsers,pnetworkUsers,p3g4gUsers) VALUES (?,?,?,?,?,?,?)";
    PreparedStatement stmt1 = conn.prepareStatement(sql);
    stmt1.setString(1, "Mobilink");
    stmt1.setInt(2, m3);
    stmt1.setInt(3, m2);
    stmt1.setInt(4, m1);
    stmt1.setInt(5, m4);
    stmt1.setInt(6, m5);
    stmt1.setInt(7, m6);
    stmt1.addBatch();
    stmt1.setString(1, "Telenor");
    stmt1.setInt(2, t3);
    stmt1.setInt(3, t2);
    stmt1.setInt(4, t1);
    stmt1.setInt(5, t4);
    stmt1.setInt(6, t5);
    stmt1.setInt(7, t6);
    stmt1.addBatch();
    stmt1.setString(1, "Ufone");
    stmt1.setInt(2, u3);
    stmt1.setInt(3, u2);
    stmt1.setInt(4, u1);
    stmt1.setInt(5, u4);
    stmt1.setInt(6, u5);
    stmt1.setInt(7, u6);
    stmt1.addBatch();
    stmt1.setString(1, "Zong");
    stmt1.setInt(2, z3);
    stmt1.setInt(3, z2);
    stmt1.setInt(4, z1);
    stmt1.setInt(5, z4);
    stmt1.setInt(6, z5);
    stmt1.setInt(7, z6);
    stmt1.addBatch();
    stmt1.setString(1, "Warid");
    stmt1.setInt(2, w3);
    stmt1.setInt(3, w2);
    stmt1.setInt(4, w1);
    stmt1.setInt(5, w4);
    stmt1.setInt(6, w5);
    stmt1.setInt(7, w6);
    stmt1.addBatch();
    stmt1.executeBatch();
    }

    public int getPostCounter(String tableName,String type) throws ClassNotFoundException, SQLException {
       //To change body of generated methods, choose Tools | Templates.
    
        Connection conn = this.gettConnection();
        String sql2 = "SELECT  count(*) from "+tableName+"";
        Statement stm2 = conn.createStatement();
        ResultSet data2 = stm2.executeQuery(sql2);
        data2.next();
        int lastIndex=data2.getInt(1);
        String sql = null;
        if(type.equals("f"))
        {
            sql = "SELECT  PID from "+tableName+" where post.index = '"+lastIndex+"'";
        } else if (type.equals("t"))
        {
            sql = "SELECT  tweet_id from "+tableName+" where twitter_data.index = '"+lastIndex+"'";
        }
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql);
        data.next();
        String conter=data.getString(1);
        System.out.println(conter);
        conter=conter.trim();
        conter=conter.substring(1,conter.length());
                System.out.println(conter);
        int counter=Integer.parseInt(conter);   // here the first parameter is inclusive and 2nd parameter in exclusive
        return counter;
    }
 // A program written to copy data from one table to othere with an increment of Index primary key colum (auto increment) in the newer table   
     public void addDuplicateData1() throws ClassNotFoundException, SQLException, IOException {
     
        Connection conn = this.gettConnection();
        String sql1 = "SELECT * FROM smf.post2";
        Statement stm = conn.createStatement();
        ResultSet data = stm.executeQuery(sql1);
        String batch = "INSERT INTO smf.post(PID, TelcoName, PostText, SharesCount, LikeCount, NofComments, CreatedTime) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(batch);
        while (data.next()) {
            // effectiveness of number of likes should be here
            stmt.setString(1, data.getString(1));
            stmt.setString(2, data.getString(2));
            stmt.setString(3, data.getString(3));
            stmt.setInt(4, data.getInt(4));
            stmt.setInt(5, data.getInt(5));
            stmt.setInt(6, data.getInt(6));
            stmt.setDate(7,data.getDate(7));
            stmt.addBatch();
        }
        stmt.executeBatch();
        //     conn.close();
     }
}
    
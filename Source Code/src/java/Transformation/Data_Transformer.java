/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Transformation;

import java.io.IOException;
import java.sql.SQLException;
import persitentstorage.dbhandler;

/**
 *
 * @author sibghat
 */
public class Data_Transformer {
//       Naive Baysian implementataion   
//public void set_lookup_tables() throws ClassNotFoundException, SQLException, IOException
//{
//     dbhandler db=dbhandler.getInstance();
//     db.populate_stopping_words();
//     db.populate_negative_words();
//     db.populate_positive_words();
//}
public void set_language_location() throws ClassNotFoundException, SQLException, IOException, Exception
{
    dbhandler db=dbhandler.getInstance();
    System.out.println("Twitter translation Started");    
    db.standard_language_location();  // function for twitter locations
    System.out.println("Facebook translation Started");
    db.standardLocationFB();           // function for facebook location and txt feature KPI's
}
  
 public void analyze_data() throws ClassNotFoundException, SQLException, IOException
 {
     dbhandler db=dbhandler.getInstance();
     System.out.println("Twitter Analyzing Started");     
     db.semantic_Analysis();   // twiter semantic analysis
     System.out.println("Facebook Analyzing Started");     
     db.semantic_AnalysisFB();   // facebook sentimental analyisis
     db.semanticPostFB();
 }
 public void populatingDimentionTables() throws ClassNotFoundException, SQLException, IOException, Exception
 {
      dbhandler db=dbhandler.getInstance();
  //    the functions here to populate data in dimention tables
      db.addLocationDimention();
      db.addTelcoDimension();
      db.addTimeDimension();
      db.addCommentDimension();
      db.addPostDimension();
     db.addFactTable();
 }
    public void businessQuestionsDatapopulation() throws ClassNotFoundException, SQLException, IOException, Exception
   {
         dbhandler db=dbhandler.getInstance();
   // call your functions to answer the question here
         db.bqServiceSatisfaction(); // Q 1
        db.bqSocialMediaTraffic();  // Q 10
         db.bqCompetitiveadvantagePrice();  //Q 4
         db.bqCompetitiveadvantageNetwork();  // Q5
         db.bqCompetitiveadvantage3G4G();   // Q6
         db.bq_bigger_network();            // Q 2
         db.bq_geographically_dominating();   // Q 3
         db.bq_monthly_trend();                 // Q 7
         db.bq_monthly_ratings();               // Q 8
         db.bqFactorDistribution();
   }
 
 public void Classifier() throws ClassNotFoundException, SQLException, IOException
{
   train t=new train();         // these calls are to train the classifiers
   t.trainModel1();              // to train N-Gram Classsifier (N=7)
   t.trainModel2();              // call to train the Bi-Gram classifier
}
}

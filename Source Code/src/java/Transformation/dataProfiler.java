/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Transformation;

import java.sql.SQLException;
import persitentstorage.dbhandler;

/**
 *
 * @author sibghat
 */
public class dataProfiler {
    String fieldName;
    String nullCount;
    String uniqueCount;

    public dataProfiler() {
    
    }

    public dataProfiler(String fieldName, String nullCount, String uniqueCount) {
        this.fieldName = fieldName;
        this.nullCount = nullCount;
        this.uniqueCount = uniqueCount;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getNullCount() {
        return nullCount;
    }

    public void setNullCount(String nullCount) {
        this.nullCount = nullCount;
    }

    public String getUniqueCount() {
        return uniqueCount;
    }

    public void setUniqueCount(String uniqueCount) {
        this.uniqueCount = uniqueCount;
    }
    
    
public String unique_values(String table_name,String column_name) throws ClassNotFoundException, SQLException
{
    dbhandler db=dbhandler.getInstance();
    System.out.println(db.count_distinct(table_name, column_name));
   return db.count_distinct(table_name, column_name);
}
 public String count_null(String table_name,String column_name) throws ClassNotFoundException, SQLException
 {
    dbhandler db=dbhandler.getInstance();
    System.out.println(db.null_values(table_name, column_name));
   return db.null_values(table_name, column_name); 
 }
}

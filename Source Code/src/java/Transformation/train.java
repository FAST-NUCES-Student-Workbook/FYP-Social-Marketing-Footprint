/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transformation;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.LMClassifier;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.util.AbstractExternalizable;
import com.aliasi.util.Compilable;
import com.aliasi.util.Files;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author sibghat
 */
public class train {

    /**
     *
     */

    public train() {
    }

    public void trainModel1() throws IOException, ClassNotFoundException {

        File data_folder;
        String[] sentiment_types;
        LMClassifier classifier;
        data_folder = new File("D:\\Practice\\Net beans\\SMF\\SMF\\trainDirectory");
        sentiment_types = data_folder.list();
        int nGram = 4;
        classifier = DynamicLMClassifier.createNGramProcess(sentiment_types, nGram);
        for (int i = 0; i < sentiment_types.length; ++i) {

            String category = sentiment_types[i];
            Classification classification = new Classification(category);
            File file = new File(data_folder, sentiment_types[i]);
            File[] trainFiles = file.listFiles();
            for (int j = 0; j < trainFiles.length; ++j) {

                File trainFile = trainFiles[j];
                String review = Files.readFromFile(trainFile, "ISO-8859-1");
                Classified classified = new Classified(review, classification);
                ((ObjectHandler) classifier).handle(classified);
            }
            System.out.println("Current Folder: " + (i + 1));
        }
        AbstractExternalizable.compileTo((Compilable) classifier, new File("D:\\Practice\\Net beans\\SMF\\SMF\\classified1.txt"));
    }

    public void trainModel2() throws IOException, ClassNotFoundException {
        File data_folder;
        String[] sentiment_types;
        LMClassifier classifier;
        data_folder = new File("D:\\Practice\\Net beans\\SMF\\SMF\\trainDirectory 2");
        sentiment_types = data_folder.list();
        int nGram = 2; //the nGram level, any value between 7 and 12 works
        classifier = DynamicLMClassifier.createNGramProcess(sentiment_types, nGram);

        for (int i = 0; i < sentiment_types.length; ++i) {
            String category = sentiment_types[i];
            Classification classification = new Classification(category);
            File file = new File(data_folder, sentiment_types[i]);
            File[] trainFiles = file.listFiles();
            for (int j = 0; j < trainFiles.length; ++j) {
                File trainFile = trainFiles[j];
                String review = Files.readFromFile(trainFile, "ISO-8859-1");
                Classified classified = new Classified(review, classification);
                ((ObjectHandler) classifier).handle(classified);
            }
            System.out.println("Current Folder: " + (i + 1));
        }
        AbstractExternalizable.compileTo((Compilable) classifier, new File("D:\\Practice\\Net beans\\SMF\\SMF\\classified2.txt"));
    }
}

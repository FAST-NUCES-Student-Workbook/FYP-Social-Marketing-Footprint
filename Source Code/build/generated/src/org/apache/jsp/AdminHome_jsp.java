package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Businesslogic.*;
import java.text.*;

public final class AdminHome_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");

 // if(!session.getAttribute("name").toString().isEmpty()) 
 // {

      out.write("\n");
      out.write("<html>\n");
      out.write("\t<head>\n");
      out.write("\t\t<title>Administrator Home</title>\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/jquery.scrollzer.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\n");
      out.write("\t\t<noscript>\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\n");
      out.write("\t\t</noscript>\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\n");
      out.write("\t</head>\n");
      out.write("\t<body>\n");
      out.write("            \n");
      out.write("\t\t<!-- Header -->\n");
      out.write("\t\t\t<div id=\"header\" class=\"skel-layers-fixed\">\n");
      out.write("\n");
      out.write("\t\t\t\t<div class=\"top\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t<!-- Logo -->\n");
      out.write("\t\t\t\t\t\t<div id=\"logo\">\n");
      out.write("\t\t\t\t\t\t\t<span class=\"image avatar48\"><img src=\"images/avatar.png\" alt=\"\" /></span>\n");
      out.write("\t\t\t\t\t\t\t<h1 id=\"title\">System Administrator</h1>\n");
      out.write("\t\t\t\t\t                                                        <p><a href=\"index.jsp\" id=\"top-link\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-sign-out\">Logout</span></a></p>\t\t\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t<!-- Nav -->\n");
      out.write("\t\t\t\t\t\t<nav id=\"nav\">\n");
      out.write("\t\t\t\t\t\t\t<!--\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\tPrologue's nav expects links in one of two formats:\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t1. Hash link (scrolls to a different section within the page)\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t\t   <li><a href=\"#foobar\" id=\"foobar-link\" class=\"icon fa-whatever-icon-you-want skel-layers-ignoreHref\"><span class=\"label\">Foobar</span></a></li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t2. Standard link (sends the user to another page/site)\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t   <li><a href=\"http://foobar.tld\" id=\"foobar-link\" class=\"icon fa-whatever-icon-you-want\"><span class=\"label\">Foobar</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t-->\n");
      out.write("\t\t\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"AdminHome.jsp\" id=\"top-link\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-home\">Home</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"ManageAccounts.jsp\" id=\"new_account\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-user\">Manage User Accounts</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"AcquireData.jsp\" id=\"acquire_data\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-download\">Acquire User Feedback</span></a></li>\n");
      out.write("                                                                <li><a href=\"ViewData.jsp\" id=\"view_data\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-eye\">View User Feedback</span></a></li>\n");
      out.write("                                                                <li><a href=\"AnalyzeData.jsp\" id=\"analyze_data\" class=\"skel-layers-ignoreHref\"><span class=\"icon fa-gear\">Analyze User Feedback</span></a></li>\n");
      out.write("                                                                \n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</nav>\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t<div class=\"bottom\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t<!-- Social Icons -->\n");
      out.write("\t\t\t\t\t\t<ul class=\"icons\">\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#\" class=\"icon fa-twitter\"><span class=\"label\">Twitter</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#\" class=\"icon fa-facebook\"><span class=\"label\">Facebook</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#\" class=\"icon fa-github\"><span class=\"label\">Github</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#\" class=\"icon fa-dribbble\"><span class=\"label\">Dribbble</span></a></li>\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#\" class=\"icon fa-envelope\"><span class=\"label\">Email</span></a></li>\n");
      out.write("\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<!-- Main -->\n");
      out.write("\t\t\t<div id=\"main\">\n");
      out.write("\n");
      out.write("\t\t\t\t<!-- Intro -->\n");
      out.write("\t\t\t\t\t<section id=\"top\" class=\"one dark cover\" style=\"height: 642.666656px; \">\n");
      out.write("\t\t\t\t\t\t<div class=\"container\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t<header>\n");
      out.write("                                            <h2 class=\"alt\"> Social<br />\n");
      out.write("\t\t\t\t\t\t\t\tMarketing FootPrint</h2>\n");
      out.write("\t\t\t\t\t\t\t\t<p>\"A brand is worthless if it doesn't CONNECT<br />\n");
      out.write("\t\t\t\t\t\t\t\twith right audiences in a relevant way\"</p>\n");
      out.write("                                        \n");
      out.write("\t\t\t\t\t\t\t</header>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</section>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<!-- Footer -->\n");
      out.write("\t\t\t<div id=\"footer\" style=\"padding-top: 0px;padding-bottom: 0px;\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t<!-- Copyright -->\n");
      out.write("\t\t\t\t\t<ul class=\"copyright\" style=\"margin-top: -35px;\">\n");
      out.write("\t\t\t\t\t\t<li>&copy; SMF. All rights reserved.</li><li>Design: <a href=\"http://html5up.net\">Y-Not Solutions</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t</div>\n");
      out.write("\n");
      out.write("\t</body>\n");
      out.write("</html>\n");

 // }

    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

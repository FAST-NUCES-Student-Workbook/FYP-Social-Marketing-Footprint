package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import Businesslogic.*;
import java.text.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title>Social Marketing Footprint - Login</title>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n");
      out.write("        <meta name=\"description\" content=\"Expand, contract, animate forms with jQuery wihtout leaving the page\" />\n");
      out.write("        <meta name=\"keywords\" content=\"expand, form, css3, jquery, animate, width, height, adapt, unobtrusive javascript\"/>\n");
      out.write("\t\t<link rel=\"shortcut icon\" href=\"../favicon.ico\" type=\"image/x-icon\"/>\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/styles.css\" />\n");
      out.write("\t\t<script src=\"js/cufon-yui.js\" type=\"text/javascript\"></script>\n");
      out.write("\t\t<script src=\"js/ChunkFive_400.font.js\" type=\"text/javascript\"></script>\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\tCufon.replace('h1',{ textShadow: '1px 1px #fff'});\n");
      out.write("\t\t\tCufon.replace('h2',{ textShadow: '1px 1px #fff'});\n");
      out.write("\t\t\tCufon.replace('h3',{ textShadow: '1px 1px #000'});\n");
      out.write("\t\t\tCufon.replace('.back');\n");
      out.write("\t\t</script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <div class=\"wrapper\">\n");
      out.write("                    <div class=\"content\">\n");
      out.write("\t\t\t\t<div id=\"form_wrapper\" class=\"form_wrapper\">\n");
      out.write("                                    <form class=\"login active\" method=\"post\" action=\"index\">\n");
      out.write("\t\t\t\t\t\t<h3>Login</h3>\n");
      out.write("\t\t\t\t\t\t<div>\n");
      out.write("\t\t\t\t\t\t\t<label>User Type:</label>\n");
      out.write("\t\t\t\t\t\t\t<input type=\"radio\" name=\"usertype\" value=\"admin\" style=\"margin-left: 30px;margin-top: 10px;margin-right:10px;\">Administrator<br>\n");
      out.write("                                                        <input type=\"radio\" name=\"usertype\" value=\"ba\" style=\"margin-left: 30px;margin-right:10px;\">Business Analyst\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("                                                <div>\n");
      out.write("\t\t\t\t\t\t\t<label>Username:</label>\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"username\">\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div>\n");
      out.write("\t\t\t\t\t\t\t<label>Password:</label>\n");
      out.write("\t\t\t\t\t\t\t<input type=\"password\" name=\"userpass\">\n");
      out.write("                                                        \n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"bottom\">\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t<input type=\"submit\" value=\"LOGIN\" name=\"submit\" >\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"clear\"></div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\n");
      out.write("\t\t<!-- The JavaScript -->\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js\"></script>\n");
      out.write("\t\t<script type=\"text/javascript\">\n");
      out.write("\t\t\t$(function() {\n");
      out.write("\t\t\t\t\t//the form wrapper (includes all forms)\n");
      out.write("\t\t\t\tvar $form_wrapper\t= $('#form_wrapper'),\n");
      out.write("\t\t\t\t\t//the current form is the one with class active\n");
      out.write("\t\t\t\t\t$currentForm\t= $form_wrapper.children('form.active'),\n");
      out.write("\t\t\t\t\t//the change form links\n");
      out.write("\t\t\t\t\t$linkform\t\t= $form_wrapper.find('.linkform');\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t//get width and height of each form and store them for later\t\t\t\t\t\t\n");
      out.write("\t\t\t\t$form_wrapper.children('form').each(function(i){\n");
      out.write("\t\t\t\t\tvar $theForm\t= $(this);\n");
      out.write("\t\t\t\t\t//solve the inline display none problem when using fadeIn fadeOut\n");
      out.write("\t\t\t\t\tif(!$theForm.hasClass('active'))\n");
      out.write("\t\t\t\t\t\t$theForm.hide();\n");
      out.write("\t\t\t\t\t$theForm.data({\n");
      out.write("\t\t\t\t\t\twidth\t: $theForm.width(),\n");
      out.write("\t\t\t\t\t\theight\t: $theForm.height()\n");
      out.write("\t\t\t\t\t});\n");
      out.write("\t\t\t\t});\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t//set width and height of wrapper (same of current form)\n");
      out.write("\t\t\t\tsetWrapperWidth();\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t/*\n");
      out.write("\t\t\t\tclicking a link (change form event) in the form\n");
      out.write("\t\t\t\tmakes the current form hide.\n");
      out.write("\t\t\t\tThe wrapper animates its width and height to the \n");
      out.write("\t\t\t\twidth and height of the new current form.\n");
      out.write("\t\t\t\tAfter the animation, the new form is shown\n");
      out.write("\t\t\t\t*/\n");
      out.write("\t\t\t\t$linkform.bind('click',function(e){\n");
      out.write("\t\t\t\t\tvar $link\t= $(this);\n");
      out.write("\t\t\t\t\tvar target\t= $link.attr('rel');\n");
      out.write("\t\t\t\t\t$currentForm.fadeOut(400,function(){\n");
      out.write("\t\t\t\t\t\t//remove class active from current form\n");
      out.write("\t\t\t\t\t\t$currentForm.removeClass('active');\n");
      out.write("\t\t\t\t\t\t//new current form\n");
      out.write("\t\t\t\t\t\t$currentForm= $form_wrapper.children('form.'+target);\n");
      out.write("\t\t\t\t\t\t//animate the wrapper\n");
      out.write("\t\t\t\t\t\t$form_wrapper.stop()\n");
      out.write("\t\t\t\t\t\t\t\t\t .animate({\n");
      out.write("\t\t\t\t\t\t\t\t\t\twidth\t: $currentForm.data('width') + 'px',\n");
      out.write("\t\t\t\t\t\t\t\t\t\theight\t: $currentForm.data('height') + 'px'\n");
      out.write("\t\t\t\t\t\t\t\t\t },500,function(){\n");
      out.write("\t\t\t\t\t\t\t\t\t\t//new form gets class active\n");
      out.write("\t\t\t\t\t\t\t\t\t\t$currentForm.addClass('active');\n");
      out.write("\t\t\t\t\t\t\t\t\t\t//show the new form\n");
      out.write("\t\t\t\t\t\t\t\t\t\t$currentForm.fadeIn(400);\n");
      out.write("\t\t\t\t\t\t\t\t\t });\n");
      out.write("\t\t\t\t\t});\n");
      out.write("\t\t\t\t\te.preventDefault();\n");
      out.write("\t\t\t\t});\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\tfunction setWrapperWidth(){\n");
      out.write("\t\t\t\t\t$form_wrapper.css({\n");
      out.write("\t\t\t\t\t\twidth\t: $currentForm.data('width') + 'px',\n");
      out.write("\t\t\t\t\t\theight\t: $currentForm.data('height') + 'px'\n");
      out.write("\t\t\t\t\t});\n");
      out.write("\t\t\t\t}\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t/*\n");
      out.write("\t\t\t\tfor the demo we disabled the submit buttons\n");
      out.write("\t\t\t\tif you submit the form, you need to check the \n");
      out.write("\t\t\t\twhich form was submited, and give the class active \n");
      out.write("\t\t\t\tto the form you want to show\n");
      out.write("\t\t\t\t*/\n");
      out.write("\t\t\t\t//$form_wrapper.find('input[type=\"submit\"]')\n");
      out.write("\t\t\t\t//\t\t\t .click(function(e){\n");
      out.write("\t\t\t\t//\t\t\t\te.preventDefault();\n");
      out.write("\t\t\t\t//\t\t\t });\t\n");
      out.write("\t\t\t});\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

<%-- 
    Document   : updateacc
    Created on : Oct 29, 2014, 3:13:07 PM
    Author     : Aqeel's
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Account</title>
    </head>
    <body>
        <h1>Update User Account!</h1>
         <form method="post" action="verify">
             <table>
                <tr><td>Enter User ID:</td><td> <input type="text" name="userid" required="true"></td></tr>
                <tr><td>Enter User Password:</td><td> <input type="password" name="userpass" required="true"></td></tr>
                <tr> <td></td><td><input type="submit" value="Search" name="submit" ></td></tr>
            </table>
        </form>
         <form method="post" action="update">
            <table>
                
                <tr> <td>ID: </td><td><input type="text" name="usrid" value="${Uid}" contenteditable="false" required="true"> </td></tr>
                <tr> <td>User Name: </td><td><input type="text" name="username" value="${Uname}" required="true"></td></tr>
                <tr><td>Password:</td><td> <input type="password" name="userpass" value="${Upass}" required="true"></td></tr>
                <tr><td>CNIC:</td><td> <input type="text" name="usercnic" value="${Ucnic}" required="true"></td></tr>
                <tr><td>Address:</td><td> <input type="text" name="useraddr" value="${Uaddress}" required="true"></td></tr>
                <tr><td>Phone:</td><td> <input type="text" name="userphone" value="${Unumber}" required="true"></td></tr>
                
                <tr> <td></td><td><input type="submit" value="Update Account" name="submit" ></td></tr>
                
            </table>
        </form>
    </body>
</html>

<%-- 
    Document   : ADMIN
    Created on : Oct 24, 2014, 11:30:34 PM
    Author     : Aqeel's
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,Businesslogic.*,java.text.*"%>
<!DOCTYPE html>

<html>
	<head>
		<title>Acquire Data</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.scrollzer.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Header -->
			<div id="header" class="skel-layers-fixed">

				<div class="top">

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="images/avatar.png" alt="" /></span>
							<h1 id="title">System Administrator</h1>
						                                                        <p><a href="index.jsp" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-sign-out">Logout</span></a></p>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<!--
							
								Prologue's nav expects links in one of two formats:
								
								1. Hash link (scrolls to a different section within the page)
								
								   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

								2. Standard link (sends the user to another page/site)

								   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
							
							-->
							<ul>
								<li><a href="AdminHome.jsp" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Home</span></a></li>
								<li><a href="ManageAccounts.jsp" id="new_account" class="skel-layers-ignoreHref"><span class="icon fa-user">Manage User Accounts</span></a></li>
								<li><a href="AcquireData.jsp" id="acquire_data" class="skel-layers-ignoreHref"><span class="icon fa-download">Acquire User Feedback</span></a></li>
                                                                <li><a href="ViewData.jsp" id="view_data" class="skel-layers-ignoreHref"><span class="icon fa-eye">View User Feedback</span></a></li>
                                                                <li><a href="AnalyzeData.jsp" id="analyze_data" class="skel-layers-ignoreHref"><span class="icon fa-gear">Analyze User Feedback</span></a></li>
                                                                
							</ul>
						</nav>
						
				</div>
				
				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>
				
				</div>
			
			</div>

		<!-- Main -->
			<div id="main">

			                    
				<!-- Acquire Data -->
					<section id="AcquireData" class="four" style="height: 642.666656px; ">
						<div class="container">

							<header>
								<h2><span class="icon fa-download">  Acquire User Feedback</span></h2>
							</header>

                                                    <p>
                                                        Enter keywords to crawl data from social media.
                                                    </p>
							
							<form onsubmit="document.getElementById('loading').style.display='block'" method="post" action="AcquireData">
								<div class="row 50%">
									<div class="6u"><input type="text" name="keyword" placeholder="Keyword" style="left: 95px; width: 376px;"/></div>
                                                                      
                                                                        
									
								</div>
                                                            <div class="row 50%">	
                                                                <div class="6u">
                                                                    <table>
                                                                        <tr><td>From :</td><td><input type="date" name="date1" /></td></tr>
                                                                        <tr><td>To :</td><td><input type="date" name="date2" /> </td></tr>
                                                                    </table>
                                                                </div>
                                	
								</div>
								<div class="row">
                                                                    <table align="center">
                                                                               <tr align="center"><td align="center">
                                                                            <img id="loading" src="loading.gif" style="display: none; " />
                                                                               </td></tr>
                                                                           </table>
									<div class="12u">
                                                                            
                                                                           
                                        <input type="submit" id="submit" value="Acquire Data" />
									</div>
								</div>
							</form>
                     

						</div>
					</section>
            
			
			</div>

		<!-- Footer -->
<div id="footer" style="padding-top: 0px;padding-bottom: 0px;">
				
				<!-- Copyright -->
					<ul class="copyright" style="margin-top: -35px;">
						<li>&copy; SMF. All rights reserved.</li><li>Design: <a href="http://html5up.net">Y-Not Solutions</a></li>
					</ul>
				
			</div>
	</body>
</html>

<%-- 
    Document   : ADMIN
    Created on : Oct 24, 2014, 11:30:34 PM
    Author     : Aqeel's
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,Businesslogic.*,java.text.*"%>
<!DOCTYPE html>

<html>
	<head>
		<title>Administrator Home</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.scrollzer.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Header -->
			<div id="header" class="skel-layers-fixed">

				<div class="top">

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="images/avatar.jpg" alt="" /></span>
							<h1 id="title">Hello Aqeel!</h1>
							<p>System Administrator</p>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<!--
							
								Prologue's nav expects links in one of two formats:
								
								1. Hash link (scrolls to a different section within the page)
								
								   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

								2. Standard link (sends the user to another page/site)

								   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
							
							-->
							<ul>
								<li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Intro</span></a></li>
								<li><a href="#CreateAccount" id="new_account" class="skel-layers-ignoreHref"><span class="icon fa-user">Create User Account</span></a></li>
                                                                <li><a href="#UpdateAccount" id="update_account" class="skel-layers-ignoreHref"><span class="icon fa-edit">Update User Account</span></a></li>
                                                                <li><a href="#DeleteAccount" id="delete_account" class="skel-layers-ignoreHref"><span class="icon fa-times">Delete User Account</span></a></li>
								<li><a href="AcquireData.jsp" id="acquire_data" class="skel-layers-ignoreHref"><span class="icon fa-download">Acquire User Feedback</span></a></li>
                                                                <li><a href="ViewData.jsp" id="view_data" class="skel-layers-ignoreHref"><span class="icon fa-eye">View User Feedback</span></a></li>
                                                                <li><a href="AnalyzeData.jsp" id="analyze_data" class="skel-layers-ignoreHref"><span class="icon fa-gear">Analyze User Feedback</span></a></li>
                                                                
							</ul>
						</nav>
						
				</div>
				
				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>
				
				</div>
			
			</div>

		<!-- Main -->
			<div id="main">

				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2 class="alt"> responsive<br />
								site template designed by HTML5 UP.</h2>
								<p>Ligula scelerisque justo sem accumsan diam quis<br />
								vitae natoque dictum sollicitudin elementum.</p>
							</header>

						</div>
					</section>
				
                                <!-- Create User Account -->
					<section id="CreateAccount" class="three">
						<div class="container">

							<header>
								<h2><span class="icon fa-user">  Create User Account</span></h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="addacc">
								<div class="row 50%">
									<div class="6u"><input type="text" name="username" placeholder="User Name" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
                                                                        <div class="6u"><input type="text" name="usercnic" placeholder="CNIC" /></div>
                                                                        <div class="6u"><input type="text" name="useraddr" placeholder="Address" /></div>
                                                                        <div class="6u"><input type="text" name="userphone" placeholder="Phone" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Create Account" />
									</div>
								</div>
							</form>

						</div>
					</section>
                                
                                <!-- Update User Account -->
					<section id="UpdateAccount" class="three">
						<div class="container">

							<header>
								<h2><span class="icon fa-edit">  Update User Account</span></h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="verify">
								<div class="row 50%">
									<div class="6u"><input type="text" name="userid" placeholder="User ID" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Verify User" />
									</div>
								</div>
							</form>
                                                    <br>
                                                    <form method="post" action="update">
								<div class="row 50%">
                                                                    <div class="6u"><input type="text" name="userid" value="${Uid}" placeholder="User ID" /></div>
									<div class="6u"><input type="text" name="username" value="${Uname}" placeholder="User Name" /></div>
									<div class="6u"><input type="password" name="userpass" value="${Upass}" placeholder="Password" /></div>
                                                                        <div class="6u"><input type="text" name="usercnic" value="${Ucnic}" placeholder="CNIC" /></div>
                                                                        <div class="6u"><input type="text" name="useraddr" value="${Uaddress}" placeholder="Address" /></div>
                                                                        <div class="6u"><input type="text" name="userphone" value="${Unumber}" placeholder="Phone" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Update Account" />
									</div>
								</div>
							</form>

						</div>
					</section>
                                
                                <!-- Delete User Account -->
					<section id="DeleteAccount" class="three">
						<div class="container">

							<header>
								<h2><span class="icon fa-times">  Delete User Account</span></h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="delete">
								<div class="row 50%">
									<div class="6u"><input type="text" name="userid" placeholder="User ID" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
                                                                        
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Delete" />
									</div>
								</div>
							</form>

						</div>
					</section>
                                
			
			</div>

		<!-- Footer -->
			<div id="footer">
				
				<!-- Copyright -->
					<ul class="copyright">
						<li>&copy; SMF. All rights reserved.</li><li>Design: <a href="http://html5up.net">Y-Not Solutions</a></li>
					</ul>
				
			</div>

	</body>
</html>

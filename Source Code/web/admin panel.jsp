<%-- 
    Document   : ADMIN
    Created on : Oct 24, 2014, 11:30:34 PM
    Author     : Aqeel's
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,Businesslogic.*,java.text.*"%>
<!DOCTYPE html>

<html>
	<head>
		<title>Administrator Home</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.scrollzer.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>
 
		<!-- Header -->
			<div id="header" class="skel-layers-fixed">

				<div class="top">

					<!-- Logo -->
						<div id="logo" style="text-decoration:none">
							<span class="image avatar48" style="background-color:transparent;"><img src="images/avatar.png"  alt="" /></span>
							<h1 id="title">Welcome!</h1>
							<p>System Administrator</p>
                                                        <p><a href="index.jsp" style="text-decoration:none"> Log Out</a></p>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<!--
							
								Prologue's nav expects links in one of two formats:
								
								1. Hash link (scrolls to a different section within the page)
								
								   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

								2. Standard link (sends the user to another page/site)

								   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
							
							-->
							<ul>
								<li><a href="#top" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Home</span></a></li>
								<li><a href="#CreateAccount" id="portfolio-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-th">Create User Account</span></a></li>
                                                                <li><a href="#UpdateAccount" id="portfolio-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-th">Update User Account</span></a></li>
                                                                <li><a href="#DeleteAccount" id="portfolio-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-th">Delete User Account</span></a></li>
                                                                <li><a href="#ViewAccount" id="portfolio-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-th">View User Account</span></a></li>
								<li><a href="#AcquireData" id="contact-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-envelope">Acquire User Feedback</span></a></li>
                                                                <li><a href="#ViewData" id="contact-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-envelope">View Data Profiling</span></a></li>
                                                                <li><a href="#AnalyzeData" id="contact-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-envelope">Analyze User Feedback</span></a></li>
                                                                <li><a href="#ViewAnalyzedData" id="contact-link" class="skel-layers-ignoreHref" style="padding-top: 2px;padding-bottom: 2px;"><span class="icon fa-envelope">View Analyzed Data</span></a></li>
                                                                
							</ul>
						</nav>
						
				</div>
				
				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>
				
				</div>
			
			</div>

		<!-- Main -->
			<div id="main">

				<!-- Intro -->
					<section id="top" class="one dark cover">
						<div class="container">

							<header>
								<h2 class="alt"> Social<br />
								Marketing FootPrint</h2>
								<p>"A brand is worthless if it doesn't CONNECT<br />
								with right audiences in a relevant way"</p>
							</header>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
						</div>
					</section>
				
                                <!-- Create User Account -->
					<section id="CreateAccount" class="three">
						<div class="container">

							<header>
								<h2>Create User Account</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="addacc">
								<div class="row 50%">
									<div class="6u"><input type="text" name="username" placeholder="User Name" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
                                                                        <div class="6u"><input type="text" name="usercnic" placeholder="CNIC" /></div>
                                                                        <div class="6u"><input type="text" name="useraddr" placeholder="Address" /></div>
                                                                        <div class="6u"><input type="text" name="userphone" placeholder="Phone" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Create Account" />
									</div>
								</div>
							</form>

						</div>
					</section>
                                
                                <!-- Update User Account -->
					<section id="UpdateAccount" class="three">
						<div class="container">

							<header>
								<h2>Update User Account</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="verify">
								<div class="row 50%">
									<div class="6u"><input type="text" name="userid" placeholder="User ID" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Verify User" />
									</div>
								</div>
							</form>
                                                    <br>
                                                    <form method="post" action="update">
								<div class="row 50%">
                                                                    <div class="6u"><input type="text" name="userid" value="${Uid}" placeholder="User ID" /></div>
									<div class="6u"><input type="text" name="username" value="${Uname}" placeholder="User Name" /></div>
									<div class="6u"><input type="password" name="userpass" value="${Upass}" placeholder="Password" /></div>
                                                                        <div class="6u"><input type="text" name="usercnic" value="${Ucnic}" placeholder="CNIC" /></div>
                                                                        <div class="6u"><input type="text" name="useraddr" value="${Uaddress}" placeholder="Address" /></div>
                                                                        <div class="6u"><input type="text" name="userphone" value="${Unumber}" placeholder="Phone" /></div>
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Update Account" />
									</div>
								</div>
							</form>

						</div>
					</section>
                                
                                <!-- Delete User Account -->
					<section id="DeleteAccount" class="three">
						<div class="container">

							<header>
								<h2>Delete User Account</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="delete">
								<div class="row 50%">
									<div class="6u"><input type="text" name="userid" placeholder="User ID" /></div>
									<div class="6u"><input type="password" name="userpass" placeholder="Password" /></div>
                                                                        
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Delete" />
									</div>
								</div>
							</form>
                                                             <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
						</div>
					</section>
                                
                                <!-- View Users -->
					<section id="ViewAccount" class="four">
						<div class="container">

							<header>
								<h2>View User Accounts</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="ViewAccount">
								<div class="row 50%">
								
									
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="View Accounts" />
									</div>
								</div>
							</form>
                                                    <br/>
                                                   <table border="3"  cellpadding="10" bordercolor="black">
                                                       <tr><TH><H1><b><u>ID</u></b></h1></th><TH><H1><b><u>NAME</u></b></h1></th><TH><H1><b><u>PASSWORD</u></b></h1></th><TH><H1><b><u>CNIC</u></b></h1></th><TH><H1><b><u>ADDRESS</u></b></h1></th><TH><H1><b><u>PHONE</u></b></h1></th></tr>  
                                                    <c:forEach items="${UserAccounts}" var="result">
                                                            <tr>
                                                                  <td> <c:out value="${result.getid()}"/></td>
                                                                  <td> <c:out value="${result.getname()}"/></td>
                                                                  <td> <c:out value="${result.getpassword()}"/></td>
                                                                  <td> <c:out value="${result.getcnic()}"/></td>
                                                                  <td> <c:out value="${result.getaddress()}"/></td>
                                                                  <td> <c:out value="${result.getnumber()}"/></td>
                                                            </tr>
                                                     </c:forEach>
                                                   </table>
                                                     <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
						</div>
					</section>
                                
                                
                                
				<!-- Acquire Data -->
					<section id="AcquireData" class="four">
						<div class="container">

							<header>
								<h2>Acquire User Feedback</h2>
							</header>

                                                    <p>
                                                        Enter keywords to crawl data from social media.
                                                    </p>
							
							<form method="post" action="AcquireData">
								<div class="row 50%">
									<div class="6u"><input type="text" name="keyword" placeholder="Keyword" /></div>
									
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Acquire Data" />
									</div>
								</div>
							</form>
                                                     <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                        <br/>
                                                       <br/>
                                                       
						</div>
					</section>
                                
                                <!-- Data Profiling -->
					<section id="ViewData" class="four">
						<div class="container">

							<header>
								<h2>View Data Profiling</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="ProfileData">
								<div class="row 50%">
								
									
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Profile Data" />
									</div>
								</div>
							</form>
                                                    <br/>
                                                   <table border="1">
  
                                                    <c:forEach items="${ProfilingResult}" var="result">
                                                            <tr>
                                                                  <td><input name='<c:out value="${result}"/>_text' type="text" value='<c:out value="${result}"/>' style="width: 350px;"></td>
                                                            </tr>
                                                     </c:forEach>
                                                   </table>
                                                     <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                        <br/>
                                                       <br/>
                                                       
						</div>
					</section>
                                
                                <!-- Analyze Data -->
					<section id="AnalyzeData" class="four">
						<div class="container">

							<header>
								<h2>Analyze User Feedback</h2>
							</header>

                                                    <p>
                                                        Analyze user feedback acquired from social media.
                                                    </p>
							
							<form method="post" action="AnalyzeData">
								<div class="row 50%">
								
									
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="Analyze Data" />
									</div>
								</div>
							</form>
                                                     <br/>
                                                       <br/>
                                                       <br/>
                                                       <br/>
                                                        <br/>
                                                       <br/>
                                                       
						</div>
					</section>
                                
                                 <!-- View Transformed Data -->
					<section id="ViewAnalyzedData" class="four">
						<div class="container">

							<header>
								<h2>View Analyzed Data</h2>
							</header>

                                                    <p>
                                                        
                                                    </p>
							
							<form method="post" action="ViewTData">
								<div class="row 50%">
								
									
								</div>
								<div class="row">
									<div class="12u">
										<input type="submit" value="View Analyzed Data" />
									</div>
								</div>
							</form>
                                                    <br>
                                                    <H5><u>TWEETS</u></H5>
                                                    <br>
                                                   <table border="3"  cellpadding="10" bordercolor="black">
                                                       <tr><TH><H1><b>TEXT</b></h1></th><TH><H1><b>SEMANTIC</b></h1></th></tr>  
                                                    <c:forEach items="${TWEETS}" var="result"> 
                                                            <tr>
                                                                  <td> <c:out value="${result.get(1)}"/></td>
                                                                  <td> <c:out value="${result.get(8)}"/></td>      
                                                            </tr>    
                                                     </c:forEach>
                                                   </table>
                                                    <br>
                                                    <H5><u>COMMENTS</u></H5>
                                                    <br>
                                                   <table border="3"  cellpadding="10" bordercolor="black">
                                                       <tr><TH><H1><b>TEXT</b></h1></th><TH><H1><b>SEMANTIC</b></h1></th></tr>  
                                                    <c:forEach items="${COMMENTS}" var="result"> 
                                                            <tr>
                                                                  <td> <c:out value="${result.get(0)}"/></td>
                                                                  <td> <c:out value="${result.get(1)}"/></td>      
                                                            </tr>    
                                                     </c:forEach>
                                                   </table>
                                                    <br>
                                                    <H5><u>POSTS</u></H5>
                                                    <br>
                                                   <table border="3"  cellpadding="10" bordercolor="black">
                                                       <tr><TH><H1><b>TEXT</b></h1></th><TH><H1><b>POSITIVE</b></h1></th><TH><H1><b>NEGATIVE</b></h1></th></tr>  
                                                    <c:forEach items="${POSTS}" var="result"> 
                                                            <tr>
                                                                  <td> <c:out value="${result.get(0)}"/></td>
                                                                  <td> <c:out value="${result.get(1)}"/></td> 
                                                                  <td> <c:out value="${result.get(2)}"/></td>     
                                                            </tr>    
                                                     </c:forEach>
                                                   </table>

						</div>
					</section>
			
			</div>

		<!-- Footer -->
			<div id="footer">
				
				<!-- Copyright -->
					<ul class="copyright">
						<li>&copy; SMF. All rights reserved.</li><li>Design: <a href="http://html5up.net">Y-Not Solutions</a></li>
					</ul>
				
			</div>

  


	</body>
</html>

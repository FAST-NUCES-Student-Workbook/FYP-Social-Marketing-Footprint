<%-- 
    Document   : ADMIN
    Created on : Oct 24, 2014, 11:30:34 PM
    Author     : Aqeel's
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,Businesslogic.*,Transformation.*,java.text.*"%>
<!DOCTYPE html>

<html>
	<head>
		<title>View Data</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.scrollzer.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
                
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
                        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
                        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
                <style type="text/css">

.paging-nav {
  text-align: right;
  padding-top: 2px;
  font-size: 0.8em;
  
}

.paging-nav a {
  margin: auto 1px;
  font-size: 0.8em;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #8ebebc;
  color: white;
  border-radius: 3px;
  
}

.paging-nav .selected-page {
  background: #4B8683;
  font-weight: bold;
  font-size: 0.8em;
}

.paging-nav,
#tableData {
  width: 400px;
  margin: 0 auto;
  font-family: Arial, sans-serif;
  font-size: 0.8em;
  color: black;
}
#tableHead, 
#tableBody {
    border: 1px solid #4B8683;
    
}
</style>
	</head>
	<body>

		<!-- Header -->
			<div id="header" class="skel-layers-fixed">

				<div class="top">

					<!-- Logo -->
						<div id="logo">
							<span class="image avatar48"><img src="images/avatar.png" alt="" /></span>
							<h1 id="title">System Administrator</h1>
                                                        <p><a href="index.jsp" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-sign-out">Logout</span></a></p>
						</div>

					<!-- Nav -->
						<nav id="nav">
							<!--
							
								Prologue's nav expects links in one of two formats:
								
								1. Hash link (scrolls to a different section within the page)
								
								   <li><a href="#foobar" id="foobar-link" class="icon fa-whatever-icon-you-want skel-layers-ignoreHref"><span class="label">Foobar</span></a></li>

								2. Standard link (sends the user to another page/site)

								   <li><a href="http://foobar.tld" id="foobar-link" class="icon fa-whatever-icon-you-want"><span class="label">Foobar</span></a></li>
							
							-->
							<ul>
								<li><a href="AdminHome.jsp" id="top-link" class="skel-layers-ignoreHref"><span class="icon fa-home">Home</span></a></li>
								<li><a href="ManageAccounts.jsp" id="new_account" class="skel-layers-ignoreHref"><span class="icon fa-user">Manage User Accounts</span></a></li>
								<li><a href="AcquireData.jsp" id="acquire_data" class="skel-layers-ignoreHref"><span class="icon fa-download">Acquire User Feedback</span></a></li>
                                                                <li><a href="ViewData.jsp" id="view_data" class="skel-layers-ignoreHref"><span class="icon fa-eye">View User Feedback</span></a></li>
                                                                <li><a href="AnalyzeData.jsp" id="analyze_data" class="skel-layers-ignoreHref"><span class="icon fa-gear">Analyze User Feedback</span></a></li>
                                                                
							</ul>
						</nav>
						
				</div>
				
				<div class="bottom">

					<!-- Social Icons -->
						<ul class="icons">
							<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
							<li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon fa-envelope"><span class="label">Email</span></a></li>
						</ul>
				
				</div>
			
			</div>

		<!-- Main -->
			<div id="main">

				
                                <!-- View Data -->
					<section id="ViewData" class="four" style="height: 642.666656px; ">
						<div class="container">

							<header>
								<h2><span class="icon fa-eye">  View User Feedback</span></h2>
							</header>

                                                    <p>
                                                        <c:if test="${not empty list}">
                                                        <table id="tableData" class="table table-bordered table-striped" >
          <thead id="tableHead">
              <tr >
              <th id="tableHead">Data Type</th>
              <th id="tableHead">Null Count</th>
              <th id="tableHead">Unique Count</th>
              
            </tr>
  </thead>
          <tbody id="tableBody">
            
            
                 <c:forEach items="${list}" var="result"> 
                    <tr id="tableHead">
                          <td id="tableHead"> <c:out value="${result.get(0)}"/></td>
                          <td id="tableHead"> <c:out value="${result.get(1)}"/></td>      
                          <td id="tableHead"> <c:out value="${result.get(2)}"/></td>      
                    </tr>    
                 </c:forEach>  
               </c:if>
            
  </tbody>
        </table>
                                                    </p>
							
							
							<form onsubmit="document.getElementById('loading').style.display='block'" method="post" action="ViewData">
								<div class="row 50%">
								
									
								</div>
                                                            
									
                                                                           <table align="center">
                                                                               <tr align="center"><td align="center">
                                                                            <img id="loading" src="loading.gif" style="display: none; " />
                                                                               </td></tr>
                                                                           </table>
									
							
								<div class="row">
									<div class="12u">
                                                                        
										<input type="submit" value="View Data" />
									</div>
								</div>
							</form>
						</div>
					</section>
			
			</div>

		<!-- Footer -->
	<div id="footer" style="padding-top: 0px;padding-bottom: 0px;">
				
				<!-- Copyright -->
					<ul class="copyright" style="margin-top: -35px;">
						<li>&copy; SMF. All rights reserved.</li><li>Design: <a href="http://html5up.net">Y-Not Solutions</a></li>
					</ul>
				
			</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="paging.js"></script> 
<script type="text/javascript">
            $(document).ready(function() {
                $('#tableData').paging({limit:5});
            });
        </script>
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
	</body>
</html>
